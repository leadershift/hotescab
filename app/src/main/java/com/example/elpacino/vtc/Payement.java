package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by elpacino on 07/09/16.
 */
public class Payement extends Fragment {

    JSONObject user,orderobj;
    double totalprice;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.paiement, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState) {

        TextView nom=(TextView)view.findViewById(R.id.nom);
        TextView adr_depart=(TextView)view.findViewById(R.id.adr_depart);
        TextView adr_dest=(TextView)view.findViewById(R.id.adr_dest);
        TextView prix_fixe=(TextView)view.findViewById(R.id.prix_fixe);
        TextView prix_total=(TextView)view.findViewById(R.id.prix_total);
        TextView supplement=(TextView)view.findViewById(R.id.supplement);
        TextView code=(TextView)view.findViewById(R.id.recep_code);
        Button probleme=(Button)view.findViewById(R.id.prob);
        Button add_dest=(Button)view.findViewById(R.id.add_dest);

        SharedPreferences preferences =getActivity().getSharedPreferences("pref", MODE_PRIVATE);
        String str = preferences.getString("user", "");
        try {
            JSONObject obj=new JSONObject(str);
           // code.setText(obj.getString("code"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Bundle bundle = getArguments();

        if (bundle!=null) {

            try {


                Log.i("mybundledata", bundle.toString());


                Log.i("my_data", bundle.getString("my_data", "default"));
                String my_data = bundle.getString("my_data", "default");

                JSONObject obj=new JSONObject(my_data);
                orderobj=new JSONObject(my_data);
                Log.i("my_data",obj.toString());
                user=new JSONObject(obj.getString("user"));

                String name = "";
                if (user.has("voyageur"))
                    name = user.getJSONObject("voyageur").getString("firstname") + " " + user.getJSONObject("voyageur").getString("lastname");
                else
                    name = user.getJSONObject("receptif").getString("firstname") + " " + user.getJSONObject("receptif").getString("lastname");

                nom.setText(name);
                //
                float prix=Float.parseFloat(user.getString("prix"));

                if(prix < Float.parseFloat(user.getString("minPrice")))
                        prix = (float) (Float.parseFloat(user.getString("minPrice"))*1.2);



                String x =bundle.getString("attenteprix", "default");
                Log.i("attenteprix",x);

                float attente=Float.parseFloat( x);
                float pricetotal=Float.parseFloat( bundle.getString("pricetotal", "default"));
                float fixe = Float.parseFloat(bundle.getString("old", "default"));

                if(pricetotal < Float.parseFloat(user.getString("minPrice"))*1.2){
                    pricetotal = (float) (Float.parseFloat(user.getString("minPrice"))*1.2);
                    fixe = (float) (Float.parseFloat(user.getString("minPrice")) * 1.2);
                    attente = (float) 0.0;
                }

               totalprice = pricetotal;

                prix_fixe.setText(String.format("%.2f", fixe)+" €");
                supplement.setText(String.format("%.2f", attente)+" €");
                prix_total.setText(String.format("%.2f", pricetotal)+" €");

                adr_depart.setText(user.getString("adresse_dep").toString());
                adr_dest.setText(obj.getString("address_dest").toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            probleme.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    {
                        try {

                            SignalerProbleme fragg = new SignalerProbleme();
                            Bundle bundle=new Bundle();

                            bundle.putString("offre_id",""+user.getInt("order_id"));

                            fragg.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragg).commit();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            add_dest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    {
                        try {

                            Add_new_dest fragg = new Add_new_dest();
                            Bundle bundle=new Bundle();

                            bundle.putString("order_id",""+user.getInt("order_id"));
                            bundle.putString("frompaiment", "1");
                            bundle.putString("orderobj",""+orderobj.getJSONObject("user").toString());
                            bundle.putString("adrdeppat",""+orderobj.getJSONObject("user").getString("adresse_arr"));

                            fragg.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragg).addToBackStack(null).commit();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            view.findViewById(R.id.payment).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    {
                        payment();

                    }
                }
            });



        }
    }

    public void payment(){
        RequestParams params = new RequestParams();

     /*   params.put("order_id", offre_id);
        params.put("adr_arrive", address);
        params.put("lat_arr", lat);
        params.put("long_arr", lon);*/
        Log.i("params_express",user.toString()+"");

        try {
            params.put("order_id", user.getString("order_id"));
            params.put("price", totalprice);
            params.put("minprice", user.getString("minPrice"));

            Log.i("params_express",params.toString());
            Log.i("params_express",user.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }



        VTCRestClient.postLogin(getActivity(),"SomePayment", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    JSONObject obj=new JSONObject(s);
                    if (obj.getString("result").toString().equals("success")){


                        SharedPreferences mySPrefs = Payement.this.getActivity().getSharedPreferences("pref",MODE_PRIVATE);
                        SharedPreferences.Editor editor = mySPrefs.edit();
                        editor.remove("course");
                        editor.commit();

                        AccueilActivity frag = new AccueilActivity();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, frag);
                        //fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commitAllowingStateLoss();
                       // getActivity().getFragmentManager().popBackStack();

                    }else {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Echec!")
                                .setMessage("Une erreur s'est produite, veuillez réessayer.")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .show();

                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                if (statusCode==401)
                    new android.app.AlertDialog.Builder(getActivity())
                            .setTitle("Attention")
                            .setMessage("Votre session a expiré, veuillez reconnecter")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing

                                    Intent i=new Intent(getActivity(),LoginActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                else
                    new android.app.AlertDialog.Builder(getActivity())
                            .setTitle("Echec")
                            .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
            }

        });

    }
}