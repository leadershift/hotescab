package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by elpacino on 12/07/16.
 */
public class HistoriqueActivity extends Fragment {
    JSONObject obj;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.historique, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState) {     // Defines the xml file for the fragment


        Log.i("im in activity","");
        Button hist_jour=(Button)view.findViewById(R.id.historique);
        Button hist_hebd=(Button)view.findViewById(R.id.ebdomadaire);
        Button hist_mensuel=(Button)view.findViewById(R.id.mensuel);

        final Bundle b=new Bundle();

        final Bundle bundle = getArguments();
        if (bundle!=null){
            String myHistory = bundle.getString("hist","default");
            Log.i("myHistory",myHistory);
            try {
                obj=new JSONObject(myHistory);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        hist_jour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("mmmmmmmmmmm","click jour");

                HistoriqueJourAvtivity fragg = new HistoriqueJourAvtivity();
                Bundle bundle=new Bundle();
                //  HistoriqueJourAvtivity frag = new HistoriqueJourAvtivity();
                try {
                    if (obj!=null)
                        Log.i("5555555",""+obj);
                        if (obj.has("day")){
                            bundle.putString("day",obj.getString("day"));

                            fragg.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragg).commit();

                        }else{
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Info")
                                    .setMessage("Pas d'historique pour cette journée")
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                            //getActivity().getFragmentManager().popBackStack();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                // fragmentTransaction.replace(R.id.container, frag);
                // fragmentTransaction.addToBackStack(null);
                //fragmentTransaction.commit();

            }
        });

        hist_hebd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HistoriqueOtherActivity fragg = new HistoriqueOtherActivity();
                Bundle bundle=new Bundle();
                // HistoriqueOtherActivity frag = new HistoriqueOtherActivity();

                try {
                    bundle.putInt("tag", 1);
                    if (obj!=null)
                        if (obj.has("week")){
                            bundle.putString("week",obj.getString("week"));
                        }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                fragg.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragg).commit();
                /*frag.setArguments(b);

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, frag);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();*/
            }
        });
        hist_mensuel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                HistoriqueOtherActivity fragg = new HistoriqueOtherActivity();
                Bundle bundle=new Bundle();

                try {
                    if (obj!=null)
                        if (obj.has("month")){
                            bundle.putString("month",obj.getString("month"));
                        }

                    bundle.putInt("tag",2);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                fragg.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragg).commit();
                // frag.setArguments(b);
                // FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                //  fragmentTransaction.replace(R.id.container, frag).addToBackStack( "tag" ).commit();;
                // fragmentTransaction.addToBackStack(null);
                // fragmentTransaction.commit();
            }
        });

    }

}