package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Fragment;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by elpacino on 02/08/16.
 */
public class DemandeReservation extends Fragment {
    private DonutProgress donutprogress;
    private Timer timer;
    JSONObject dataobj;
    CountDownTimer mCountDownTimer;
    ProgressDialog progressDialog;
    int i=0;
    LocationManager locationManager;
    String lat,lng,timeClicked;
    JSONObject newpos = new JSONObject();
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    interface TaskCallbacks {
        void onPreExecute();
        void onProgressUpdate(int percent);
        void onCancelled();
        void onPostExecute();
    }

    private TaskCallbacks mCallbacks;

    private static DemandeReservation sInstance;
    public static DemandeReservation getInstance(){
        return sInstance;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.demande_reserv, parent, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {     // Defines the xml file for the fragment


        sInstance=this;

        TextView endroit= (TextView)view.findViewById(R.id.endroit);
        TextView adresse= (TextView)view.findViewById(R.id.adr);
        TextView adresse2= (TextView)view.findViewById(R.id.adr2);

        TextView recep_code= (TextView)view.findViewById(R.id.recep_code);
        Button btn5=(Button)view.findViewById(R.id.min_5);
        Button btn10=(Button)view.findViewById(R.id.min_10);
        Button btn15=(Button)view.findViewById(R.id.min_15);
        Button btn20=(Button)view.findViewById(R.id.min_20);

        final Bundle bundle = getArguments();
        if (bundle!=null){
            String demandeReservation = bundle.getString("notif_data","default");
            Log.i("Demande reservation","dataaaaaaaa :"+demandeReservation);
            try {
                dataobj=new JSONObject(demandeReservation);

                Log.i("notiiiiiiif", dataobj.toString());

                endroit.setText(dataobj.getString("firstname")+" "+dataobj.getString("lastname"));
                adresse.setText(dataobj.getString("adressedep"));

                //adresse2.setText(dataobj.getString("pays")+" "+dataobj.getString("ville")+" "+dataobj.getString("zip"));


                String time = dataobj.getString("datedep").substring(dataobj.getString("datedep").lastIndexOf(" ") + 1);

                recep_code.setText(time);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            btn5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("messageVTC","btn5 clicked");
                    progressDialog = ProgressDialog.show(DemandeReservation.this.getActivity(), "Localisation en cours", "", true);

                    timeClicked = "5";
                    //sendreptoServer(timeClicked,true, 1);
                    try {

                        newpos = MySingleton.getInstance().getPosition();

                        Log.i("fromsingleton", "&&&&&&&"+newpos);
                        calculateDistanceadresses(dataobj.getString("adressedep"),newpos.getDouble("latitude"), newpos.getDouble("longitude"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });
            btn10.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("messageVTC","btn10 clicked");
                    progressDialog = ProgressDialog.show(DemandeReservation.this.getActivity(), "Localisation en cours", "", true);

                    timeClicked = "10";
                    //sendreptoServer(timeClicked,true, 1);
                    try {

                        newpos = MySingleton.getInstance().getPosition();

                        Log.i("fromsingleton", "&&&&&&&"+newpos);
                        calculateDistanceadresses(dataobj.getString("adressedep"),newpos.getDouble("latitude"), newpos.getDouble("longitude"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });
            btn15.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("messageVTC","btn15 clicked");
                    progressDialog = ProgressDialog.show(DemandeReservation.this.getActivity(), "Localisation en cours", "", true);

                    timeClicked = "15";
                   // sendreptoServer(timeClicked,true, 1);
                    try {

                        newpos = MySingleton.getInstance().getPosition();

                        Log.i("fromsingleton", "&&&&&&&"+newpos);
                        calculateDistanceadresses(dataobj.getString("adressedep"),newpos.getDouble("latitude"), newpos.getDouble("longitude"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            btn20.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("messageVTC","btn20 clicked");
                    progressDialog = ProgressDialog.show(DemandeReservation.this.getActivity(), "Localisation en cours", "", true);

                    timeClicked = "20";
                    //sendreptoServer(timeClicked,true, 1);
                    try {

                        newpos = MySingleton.getInstance().getPosition();

                        Log.i("fromsingleton", "&&&&&&&"+newpos);
                        calculateDistanceadresses(dataobj.getString("adressedep"),newpos.getDouble("latitude"), newpos.getDouble("longitude"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        }

       // donutprogress = (DonutProgress)findViewById(R.id.donut_progress);

        final ProgressBar mProgressBar = (ProgressBar)view.findViewById(R.id.progressBar);

        final TextView count=(TextView)view.findViewById(R.id.count);
        mProgressBar.setProgress(i);
        if (MainActivity.getInstance().FromNotif){
            Date now=new Date();
            Log.i("notifDate",""+MainActivity.getInstance().notifDate.getTime());
            Log.i("now date",""+now.getTime());
            long diff= now.getTime()-MainActivity.getInstance().notifDate.getTime();
            Log.i("diff",""+diff);
            if (diff>15000){
                sendreptoServer("-1",false, 0);
            }else
                mCountDownTimer=new CountDownTimer(15000-diff,1000) {

                    @Override
                    public void onTick(long millisUntilFinished) {
                        Log.v("Log_tag", "Tick of Progress"+ i+ millisUntilFinished);

                        i++;
                        count.setText(i+" s");
                        mProgressBar.setProgress(i);
                    }

                    @Override
                    public void onFinish() {
                        //Do what you want
                        i++;
                        count.setText(i+" s");
                        mProgressBar.setProgress(i);
                        sendreptoServer("-1",false, 0);
                        Log.i("count down did finish","yes");
                    }
                };
            mCountDownTimer.start();
        }else{
            mCountDownTimer=new CountDownTimer(15000,1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    Log.v("Log_tag", "Tick of Progress"+ i+ millisUntilFinished);

                    i++;
                    count.setText(i+" s");
                    mProgressBar.setProgress(i);
                }

                @Override
                public void onFinish() {
                    //Do what you want
                    i++;
                    count.setText(i+" s");
                    mProgressBar.setProgress(i);
                    sendreptoServer("-1",false, 0);
                    Log.i("count down did finish","yes");
                }
            };
            mCountDownTimer.start();
        }


        /*ProgressBar pb = (ProgressBar)findViewById(R.id.progressBarToday);

        Animation an = new RotateAnimation(0.0f, 90.0f, 100f, 100f);
        an.setFillAfter(true);
        pb.startAnimation(an);*/

       /* timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(15 == donutprogress.getProgress()){
                            timer.cancel();

                            sendreptoServer("-1",false);
                           // finish();

                        }else
                        donutprogress.setProgress(donutprogress.getProgress() + 1);
                    }
                });
            }
        }, 1000, 500);*/

    }


    public  void  sendreptoServer(String min, final Boolean accept, int credib){

        if(mCountDownTimer != null)
            mCountDownTimer.cancel();



         RequestParams params = new RequestParams();

         SharedPreferences preferences = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
         String str = preferences.getString("user", "");
         try {
             JSONObject json = new JSONObject(str);
             params.put("user_id", json.getString("id").toString());
             params.put("order_id", dataobj.getString("order_id"));
             params.put("delais", min);
             params.put("credibilite", credib);
             params.put("cred_time", "1");

             Log.i("params",params.toString());

         }catch (JSONException e) {
                 e.printStackTrace();
             }

         Log.i("messageVTC","preparation de l'envoie ...");
         VTCRestClient.postLogin(getActivity(),"VTCReponse", params, new AsyncHttpResponseHandler() {

             @Override
             public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                 String s = null;
                 try {

                     if (progressDialog != null)
                         progressDialog.dismiss();


                     s = new String(responseBody, "UTF-8");
                     Log.i("messageVTC","Message from Server "+s);

                     JSONObject obj = new JSONObject(s);
                     String retour=obj.getString("result");

                     if (retour instanceof String){

                         if (retour.equals("success")){
                             showAlert(accept);


                         }else  if (retour.equals("failed")){
                                new AlertDialog.Builder(getActivity())
                                 .setTitle("Echec")
                                 .setMessage("contact l'administrateur")
                                 .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                     public void onClick(DialogInterface dialog, int which) {
                                         AccueilActivity frag = new AccueilActivity();
                                         FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                         fragmentTransaction.replace(R.id.container, frag);
                                         // fragmentTransaction.addToBackStack(null);
                                         fragmentTransaction.commit(); // do nothing
                                     }
                                 })
                                 .setIcon(android.R.drawable.ic_dialog_alert)
                                 .show();

                         }else  if (retour.equals("no order found")){
                             new AlertDialog.Builder(getActivity())
                                     .setTitle("Echec")
                                     .setMessage("Reservation annuler")
                                     .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                         public void onClick(DialogInterface dialog, int which) {
                                             AccueilActivity frag = new AccueilActivity();
                                             FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                             fragmentTransaction.replace(R.id.container, frag);
                                             // fragmentTransaction.addToBackStack(null);
                                             fragmentTransaction.commit(); // do nothing
                                         }
                                     })
                                     .setIcon(android.R.drawable.ic_dialog_alert)
                                     .show();

                         }else{
                             new AlertDialog.Builder(getActivity())
                                     .setTitle("Echec")
                                     .setMessage("contact l'administrateur")
                                     .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                         public void onClick(DialogInterface dialog, int which) {
                                             AccueilActivity frag = new AccueilActivity();
                                             FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                             fragmentTransaction.replace(R.id.container, frag);
                                             // fragmentTransaction.addToBackStack(null);
                                             fragmentTransaction.commit(); // do nothing
                                         }
                                     })
                                     .setIcon(android.R.drawable.ic_dialog_alert)
                                     .show();
                         }

                     }else {

                         new AlertDialog.Builder(getActivity())
                                 .setTitle("Echec")
                                 .setMessage("dataProblem")
                                 .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                     public void onClick(DialogInterface dialog, int which) {
                                         getActivity().getFragmentManager().popBackStack(); // do nothing
                                     }
                                 })
                                 .setIcon(android.R.drawable.ic_dialog_alert)
                                 .show();
                     }

                 } catch (UnsupportedEncodingException e) {
                     e.printStackTrace();
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }


             }

             @Override
             public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                 Log.i("error",error.toString());
                 Log.i("status code",""+statusCode);
                 if (progressDialog != null)
                     progressDialog.dismiss();
                 if (statusCode==401)
                     new AlertDialog.Builder(getActivity())
                             .setTitle("Attention")
                             .setMessage("Votre session a expiré, veuillez reconnecter")
                             .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int which) {
                                     // do nothing

                                     Intent i=new Intent(getActivity(),LoginActivity.class);
                                     startActivity(i);
                                     getActivity().finish();
                                 }
                             })
                             .setIcon(android.R.drawable.ic_dialog_alert)
                             .show();
                 else
                     new AlertDialog.Builder(getActivity())
                             .setTitle("Echec")
                             .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                             .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int which) {
                                     // do nothing
                                 }
                             })
                             .setIcon(android.R.drawable.ic_dialog_alert)
                             .show();
             }
         });

     }

    public void showAlert(Boolean b){
        String message;
        if (b)
            message="Votre réponse a été envoyée.";
        else
            message="Vous n'avez pas répondu à demande de réservation";


        new AlertDialog.Builder(getActivity())
                .setTitle("Info")
                .setMessage(message)
                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        AccueilActivity frag = new AccueilActivity();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, frag);
                        // fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        mCallbacks = (TaskCallbacks) activity;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("fragment distroyed","yes");

        if(timer != null)
            timer.cancel();
    }
         @Override
    public void onDestroy() {
             super.onDestroy();
            // Log.i("fragment distroyed","yes");
             if(timer != null)
                 timer.cancel();
    }
    @Override
  /*  public void onDetach(){
        super.onDetach();
        Log.i("fragment detached","yes");
        timer.cancel();
    }*/

    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        Log.i("fragment detached","yes");
    }




    void calculateDistanceadresses(final String adr1, final double lat2, final double lng2){

        if (mCountDownTimer!=null)
            mCountDownTimer.cancel();

            String adr = adr1.replace(",","");
        Log.i("adresse222",adr1);
        Log.i("adresse222",adr);
        String addressURL = "http://office-hc.fr/waze.php?depart="+adr+"&destination="+lat2+","+lng2;
        //String addressURL = "http://office-hc.fr/waze.php?depart="+adr1+"&destination=45.769248962402344,4.851263999938965";


       Log.i("adresse222",addressURL);

        Log.i("calculateDistanceadre", "11111111");

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(20000);
        client.get(addressURL, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {

                    Log.i("success","yes");
                    String s = new String(responseBody, "UTF-8");
                     Log.i("result String",s.toString());
                    JSONArray result= new JSONArray(s);
                    Log.i("wazeresult",result.toString());
                    if (progressDialog != null)
                        progressDialog.dismiss();

//                    if (result.getJSONArray("routes").length()==0) {
                    if (result.length() ==0) {


                        new AlertDialog.Builder(getActivity())
                                .setTitle("Echec!")
                                .setMessage("Aucun chemin proposé, veuillez réessayer avec une autre adresse.")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        if( MySingleton.getInstance().myMain.getFragmentManager()!=null)
                                            if (mCountDownTimer!=null)
                                                mCountDownTimer.cancel();
                                        AccueilActivity frag = new AccueilActivity();
                                        if(getFragmentManager() != null){
                                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                            fragmentTransaction.replace(R.id.container, frag);
                                            // fragmentTransaction.addToBackStack(null);
                                            fragmentTransaction.commit();// do nothing
                                        }
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .show();
                    } else{





                        Double res = (Double) result.get(0);

                        int time = Integer.parseInt(timeClicked) * 60;

                        if((time + 3600) <= (res * 60) ){
                            sendreptoServer(timeClicked,true, 1);
                        }else
                            sendreptoServer(timeClicked,true, 1);


                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    if (progressDialog != null)
                        progressDialog.dismiss();

                    new AlertDialog.Builder(DemandeReservation.this.getActivity())
                            .setTitle("Echec")
                            .setMessage("Une erreur s'est produite, waze n'as pas trouver un route ")
                            .setPositiveButton("Try again", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    progressDialog = ProgressDialog.show(DemandeReservation.this.getActivity(), "Waiting to get a route", "", true);

                                    calculateDistanceadresses(adr1, lat2, lng2);
                                }

                            })
                            .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if( MySingleton.getInstance().myMain.getFragmentManager()!=null)
                                        if (mCountDownTimer!=null)
                                            mCountDownTimer.cancel();
                                    AccueilActivity frag = new AccueilActivity();
                                    if(getFragmentManager() != null){
                                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.container, frag);
                                        // fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();// do nothing
                                    }
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                    Log.i("failure","internal error");
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
            {
                if (progressDialog != null)
                    progressDialog.dismiss();

                if ( error.getCause() instanceof ConnectTimeoutException)
                {
                    new AlertDialog.Builder(getActivity())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Echec")
                            .setMessage("Trajet non trouvé. voulez vous réessayer?")
                            .setPositiveButton("Try again", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    progressDialog = ProgressDialog.show(DemandeReservation.this.getActivity(), "Localisation en cours", "", true);

                                    calculateDistanceadresses(adr1, lat2, lng2);
                                }

                            })
                            .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();

                }


                Log.i("failure","yes  => "+statusCode);
                error.printStackTrace(System.out);
            }
        });


    }





}

