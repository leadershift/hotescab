package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by elpacino on 20/07/16.
 */
public class HistoriqueOtherActivity extends android.app.Fragment {
    HistoriqueAdapter adapter;
    JSONArray week_history,month_history;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.historique_other, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState){     // Defines the xml file for the fragment

        TextView title=(TextView)view.findViewById(R.id.text_hist);

        Bundle bundle = getArguments();
        final int myInt = bundle.getInt("tag", 1);
        Log.i("my tag",""+myInt);

        switch (myInt) {
            case 1:

                try {
                    if (bundle.containsKey("week")){
                        String weekHist = bundle.getString("week","default");
                        week_history=new JSONArray(weekHist);
                        adapter=new HistoriqueAdapter(getActivity().getBaseContext(),week_history,myInt);
                    }else {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Info")
                                .setMessage("Pas d'historique pour cette semaine")
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                        AccueilActivity frag = new AccueilActivity();
                                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.container, frag);
                                        // fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                    title.setText("Historique hebdomadaire");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            case 2:

                try {
                    if (bundle.containsKey("month")){
                        String monthHist = bundle.getString("month","default");
                        month_history=new JSONArray(monthHist);
                        adapter=new HistoriqueAdapter(getActivity().getBaseContext(),month_history,myInt);
                    }else {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Info")
                                .setMessage("Pas d'historique pour ce mois")
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                        getActivity().getFragmentManager().popBackStack();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                    }

                    title.setText("Historique Mensuel");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;

        }

        ListView list= (ListView)view.findViewById(R.id.list_jour);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position==0 || position==6 ){

                }else {
                    CourseDetailsActivity fragg = new CourseDetailsActivity();
                    Bundle bundle=new Bundle();
                    try {
                        JSONArray arr=new JSONArray();
                        switch (myInt) {
                            case 1:
                                arr=week_history;
                                break;
                            case 2:
                                arr=month_history;
                                break;
                        }

                        bundle.putString("dic",arr.get(position-1).toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    fragg.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragg).commit();
                }

            }

        });


    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.

}
