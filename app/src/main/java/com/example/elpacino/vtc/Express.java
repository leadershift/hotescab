package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by elpacino on 24/08/16.
 */
public class Express extends Fragment {

    JSONArray obj_address;

    String latitude; // latitude
    String longitude;
    JSONObject dest_coordinates;
    MyAutocompleteTextView adresse;
    ProgressDialog progressDialog;
    ArrayList<String> ar = new ArrayList<String>();
    // longitude
    CustomAutocompletationAdapter adapter;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.express, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState)  {     // Defines the xml file for the fragment

        adresse = (MyAutocompleteTextView) view.findViewById(R.id.address);
        Button valider = (Button) view. findViewById(R.id.Valider);
        Button probleme = (Button) view. findViewById(R.id.button4);

        TextView code = (TextView)  view.findViewById(R.id.recep_code);

        SharedPreferences preferences =getActivity(). getSharedPreferences("pref", Context.MODE_PRIVATE);
        String user = preferences.getString("user", "");
        try {
            JSONObject obj_user = new JSONObject(user);
            code.setText(obj_user.getString("code"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Log.i("ArrayList of adresses", ar.toString());

        adapter = new CustomAutocompletationAdapter(getActivity());

        //Set adapter to AutoCompleteTextView
        adresse.setAdapter(adapter);

        AsyncHttpClient client = new AsyncHttpClient();

        adresse.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                JSONArray res = getAddress(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

        });

        adresse.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int position, long arg3) {
                try {
                    JSONObject obj = (JSONObject) obj_address.get(position);
                    dest_coordinates = obj.getJSONObject("location");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("adresse", adresse.getText().toString());
                if (adresse.getText().toString().equals("")) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Attention!")
                            .setMessage("Vous devez saisir l'adresse de destination")
                            .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();

                } else {
                    progressDialog = ProgressDialog.show(Express.this.getActivity(), "Localisation en cours", "", true);


                    try {

Log.i("arrrrrrrrrrr", getArguments().toString());

                       String ch = getArguments().getString("data");
                        JSONObject ord = new JSONObject(ch);

                        Log.i("arrrrrrrrrrr1", obj_address.toString());
                        Log.i("arrrrrrrrrrr1", ord.toString() + "fffff");

                        calculateDistance(getArguments().getString("latitude_dep"),getArguments().getString("longitude_dep"),dest_coordinates.getString("lat"), dest_coordinates.getString("lon"));

                        //calculateDistanceadresses(ord.getString("adresse_dep"),adresse.getText().toString(),dest_coordinates.getString("lat"), dest_coordinates.getString("lon"));

                       // SendAddress(getIntent().getExtras().getString("order_id", "default"), adresse.getText().toString(), dest_coordinates.getString("lat"), dest_coordinates.getString("lon"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        });

        probleme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                {
                        SignalerProbleme fragg = new SignalerProbleme();
                        Bundle bundle=new Bundle();

                        bundle.putString("offre_id",getArguments().getString("order_id", "default"));

                        fragg.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, fragg).commit();



                }
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2 = new GoogleApiClient.Builder(getActivity()).addApi(AppIndex.API).build();
    }

    void SendAddress(String offre_id, String address, String lat, String lon,String distance,String duree)

    {

            RequestParams params = new RequestParams();

            params.put("order_id", offre_id);
            params.put("adr_arrive", address);
            params.put("lat_arr", lat);
            params.put("long_arr", lon);
            params.put("distance", distance);
            params.put("duree", duree);
            Log.i("params express",params.toString());

            VTCRestClient.postLogin(getActivity(),"addDestination", params, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                    String s = null;
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    try {
                        s = new String(responseBody, "UTF-8");
                       // Log.i("express response message", s);
                        JSONObject obj=new JSONObject(s);
                        if (obj.getString("result").toString().equals("success")){

                            Voyageur fragment=new Voyageur();
                            Bundle bundle=new Bundle();
                            bundle.putString("adr",adresse.getText().toString());
                            bundle.putString("coordinates",dest_coordinates.toString());
                            bundle.putString("price",obj.getString("prix"));
                            bundle.putString("data",getArguments().getString("data"));
                            bundle.putString("attente",getArguments().getString("attente"));
                            bundle.putString("fromExpress","no");
                            bundle.putString("notif","no");
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = Express.this.getActivity().getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment).commit();
                          //  getActivity().getFragmentManager().popBackStack();
                        }else {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Echec!")
                                    .setMessage("Une erreur s'est produite, veuillez réessayer.")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_info)
                                    .show();

                        }

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Erreur application =>  Êtes connecté au Wifi.", Toast.LENGTH_SHORT).show();
                }

            });

    }

    public JSONArray getAddress(final String address) {
        String addressURL = "";

        try {
            addressURL = "https://www.waze.com/SearchServer/mozi?q=" + URLEncoder.encode(address, "UTF-8") + ",France" + "&lang=fr&lon=2.344465&lat=48.852513&origin=livemap";
            Log.i("adresse", addressURL);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(addressURL, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String s = new String(responseBody, "UTF-8");
                    obj_address = new JSONArray(s);
                    Log.i("array", obj_address.toString());


                    adapter.RempliresultList(obj_address);
                    adapter.notifyDataSetChanged();

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable
                    error) {
                error.printStackTrace(System.out);
            }
        });

        return obj_address;

    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Express Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.elpacino.vtc/http/host/path")
        );
        AppIndex.AppIndexApi.start(client2, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Express Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.elpacino.vtc/http/host/path")
        );
        AppIndex.AppIndexApi.end(client2, viewAction);
        client2.disconnect();
    }

     void calculateDistance(String lat1, String lng1, final String lat2, final String lng2){



         String addressURL = "http://office-hc.fr/waze.php?depart="+lat1+","+lng1+"&destination="+lat2+","+lng2;
         //String addressURL = "http://app.tn/waze/waze.php?depart="+lat1+","+lng1+"&destination=45.769248962402344,4.851263999938965";



        // String addressURL = "http://maps.google.com/maps/api/directions/json?origin="+lat1+","+lng1+"&destination="+lat2+","+lng2+"&sensor=false&units=metric&key=AIzaSyBmJiKnqP5XlIogPz8zeaGp4Zccpf8O_No";
        Log.i("adresse",addressURL);

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(addressURL, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {

                    Log.i("success","yes");
                    String s = new String(responseBody, "UTF-8");
                    Log.i("result String",s.toString());
                    JSONArray result= new JSONArray(s);
                    Log.i("result",result.toString());

                    if (result.length()==0) {
                        if (progressDialog != null)
                            progressDialog.dismiss();

                       new AlertDialog.Builder(getActivity())
                                .setTitle("Echec!")
                                .setMessage("Aucun chemin proposé, veuillez réessayer avec une autre adresse.")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                       // getActivity().getFragmentManager().popBackStack(); // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .show();

                        String ch = getArguments().getString("data");
                        JSONObject ord = new JSONObject(ch);

                        Log.i("drrrrr",dest_coordinates.toString());
                       // calculateDistanceadresses(ord.getString("adresse_dep"),adresse.getText().toString(),dest_coordinates.getString("lat"), dest_coordinates.getString("lon"));

                    }
                    else{
                        progressDialog.setTitle("Trajet calculé. Envoi des informations");
 SendAddress(getArguments().getString("order_id", "default"), adresse.getText().toString(), lat2, lng2, result.get(1).toString(), result.get(0).toString());

                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable
                    error)
            {
                if (progressDialog != null)
                    progressDialog.dismiss();

                Log.i("failure","yes");
                error.printStackTrace(System.out);
            }
        });


    }


}
