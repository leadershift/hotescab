package com.example.elpacino.vtc;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elpacino on 26/08/16.
 */


public class CustomAutocompletationAdapter extends BaseAdapter implements Filterable {

    private static final int MAX_RESULTS = 10;
    private Context mContext;
    private List<String> resultList = new ArrayList<String>();

    public CustomAutocompletationAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        Log.i("resultList size",""+resultList.size());
        return resultList.size();

    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.simple_dropdown_item_2line, parent, false);
        }

        ((TextView) convertView.findViewById(R.id.text1)).setText(resultList.get(position));
        return convertView;
    }

    /**
     * Returns a search result for the given book title.
     */

    void RempliresultList(JSONArray obj_address){

        resultList.clear();
        for(int i = 0; i< obj_address.length(); i++)
        {
            try {
                JSONObject jsonObject = obj_address.getJSONObject(i);
                resultList.add(jsonObject.getString("name"));

            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

       Log.i("resultList",resultList.toString());


    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {

                    // Assign the data to the FilterResults
                    filterResults.values =resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<String>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }
}
