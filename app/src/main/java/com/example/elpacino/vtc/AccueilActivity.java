package com.example.elpacino.vtc;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by elpacino on 12/07/16.
 */
public class AccueilActivity extends Fragment {
    JSONObject user_json;
    String MesReservations;
    JSONObject myResult;
    LocationManager locationManager;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        MySingleton.getInstance().getpositionGPS(AccueilActivity.this.getActivity());

        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.accueil, parent, false);







    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        Button compte=(Button) view.findViewById(R.id.monCompte);
        Switch dispo_state = (Switch) view.findViewById(R.id.switch1);
        final TextView state=(TextView)  view.findViewById(R.id.state);

        SharedPreferences preferences = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String str = preferences.getString("user", "");
        Log.i("str",str);
        try {
            user_json = new JSONObject(str);
            getPlanning(user_json.getString("id").toString());

            Log.i("isdispo",user_json.getString("IsDisponible"));
            if (Integer.parseInt(user_json.getString("IsDisponible"))==1){

                dispo_state.setChecked(true);
                state.setText("Disponible");
            }else{

                dispo_state.setChecked(false);
                state.setText("Non disponible");

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        final Button planning=(Button) view.findViewById(R.id.planning);


        final Bundle bundle = this.getArguments();
        if (bundle!=null) {
            MesReservations = bundle.getString("reservations", "default");
            Log.i("MesReservations",MesReservations);
            if (MesReservations.equals("no reservation")){
                LinearLayout layout=(LinearLayout)view.findViewById(R.id.linearLayout);
                layout.setVisibility(View.INVISIBLE);
            }
        }

        //grab prefs first


        dispo_state.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.i("checked state",""+isChecked);
               if (isChecked){

                   state.setText("Disponible");
                   changeState(1);
               }else{
                   state.setText("Non disponible");
                   changeState(3);
               }
            }
        });

        compte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                        CompteActivity frag = new CompteActivity();
                        Bundle bundle = new Bundle();
                try {
                    bundle.putString("reservations", myResult.getString("result").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                frag.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, frag);

                        fragmentTransaction.commit();

            }
        });

        planning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {

                    MesReservations frag = new MesReservations();
                    Bundle bundle=new Bundle();
                    try {

                        if (myResult!=null)
                             bundle.putString("reservations", myResult.getString("result").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    frag.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, frag).commit();

                }
            }
        });
    }
    void changeState(final int dispo_state){

       /* HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://vtc.la2dev.com/api/ws/setDisponible");
        httppost.addHeader("Authorization", "Bearer ODJiMWJhN2JjNzAyYjFlYjhlYmViMTAwZTdjNjZkNGU0OGY0MDkzMGI5YzI2MDI0MzJhZWQ3YmU1ODk1MDI0Yw");

        httppost.setHeader("Authorization", "Bearer ODJiMWJhN2JjNzAyYjFlYjhlYmViMTAwZTdjNjZkNGU0OGY0MDkzMGI5YzI2MDI0MzJhZWQ3YmU1ODk1MDI0Yw");

        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("status", ""+dispo_state));
            nameValuePairs.add(new BasicNameValuePair("user_id", user_json.getString("id").toString()));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);


            Log.d("DEBUG", "RESPONSE: " + response);

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

       // AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        try {
            params.put("status",""+dispo_state);
            params.put("user_id", user_json.getString("id").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VTCRestClient.postLogin(getActivity(),"setDisponible", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("ws result", s);
                    JSONObject obj=new JSONObject(s);
                    if (obj.getString("result").toString().equals("success")){
                        user_json.put("IsDisponible",dispo_state);
                        android.util.Log.i("disponibility",""+dispo_state);
                        SharedPreferences preferences =getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("user", user_json.toString());
                        editor.commit();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                if (statusCode==401)
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Attention")
                            .setMessage("Votre session a expiré, veuillez reconnecter")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing

                                    Intent i=new Intent(getActivity(),LoginActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                else
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Echec")
                            .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
            }

        });

    }

    public void getPlanning(String user_id){

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("user_id",user_id);

        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "", "Loading", true);

        VTCRestClient.postLogin(getActivity(),"VTCMyReservation", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                if (progressDialog != null)
                progressDialog.dismiss();
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("Message from Server", s);

                    try {
                       myResult= new JSONObject(s);
                        if (myResult.getString("result").equals("no reservation")){
                            LinearLayout layout=(LinearLayout) AccueilActivity.this.getActivity().findViewById(R.id.linearLayout);
                            if (layout!=null)
                                layout.setVisibility(View.INVISIBLE);
                        }

                      //  activityac.loadData(myResult);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    MesReservations frag = new MesReservations();


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                if (progressDialog != null)
                progressDialog.dismiss();
                if (statusCode==401)
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Attention")
                            .setMessage("Votre session a expiré, veuillez reconnecter")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing

                                    Intent i=new Intent(getActivity(),LoginActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                else
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Echec")
                            .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
            }

        });

    }


}