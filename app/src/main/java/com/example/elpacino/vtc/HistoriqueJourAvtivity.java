package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by elpacino on 13/07/16.
 */
public class HistoriqueJourAvtivity extends Fragment {

    ListAdapter adapter;
    JSONArray myArray;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.hist_jour, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState) {     // Defines the xml file for the fragment

Log.i("historique du jour","111111111");
        TextView title=(TextView)view.findViewById(R.id.text_hist);

        final Bundle bundle = getArguments();
        if (bundle!=null){

            try {
                String myHistory = bundle.getString("day");
                if(myHistory != null){
                    myArray=new JSONArray(myHistory);
                    adapter=new ListAdapter(getActivity().getBaseContext(),myArray);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Info")
                    .setMessage("Pas d'historique pour aujourd'hui")
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                            AccueilActivity frag = new AccueilActivity();
                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.container, frag);
                            // fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        ListView list= (ListView)view.findViewById(R.id.list_jour);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position==0 || position==4 ){

                }else {

                    CourseDetailsActivity fragg = new CourseDetailsActivity();
                    Bundle bundle=new Bundle();

                    try {
                        bundle.putString("dic",myArray.get(position-1).toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    fragg.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragg).commit();
                }

            }


        });


    }


    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
}