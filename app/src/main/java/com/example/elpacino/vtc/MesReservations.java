package com.example.elpacino.vtc;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by elpacino on 03/08/16.
 */
public class MesReservations extends Fragment {
    ReservationsAdapter adapter;
    JSONArray myarray;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.prob_reserv, parent, false);
    }
    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ListView mylist= (ListView)view.findViewById(R.id.mylist);

        Button prise_en_charge=(Button)view.findViewById(R.id.prise_en_charge);
        Button probleme=(Button)view.findViewById(R.id.probleeme);

        final Bundle bundle = this.getArguments();
        if (bundle!=null){
            String myHistory = bundle.getString("reservations","default");
            Log.i("myHistory",myHistory);
            if (myHistory.equals("no reservation")){
                mylist.setVisibility(View.INVISIBLE);
                prise_en_charge.setVisibility(View.INVISIBLE);
                probleme.setVisibility(View.INVISIBLE);
                TextView message=(TextView)view.findViewById(R.id.message);
                message.setText("Pas de réservations");
                adapter=new ReservationsAdapter(getActivity(),new JSONArray());
                mylist.setAdapter(adapter);

            }else {
                try {
                    myarray=new JSONArray(myHistory);

                    adapter=new ReservationsAdapter(getActivity(),myarray);
                    adapter.selectedItem=0;
                    mylist.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        mylist.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.i("item selected",""+position);
                adapter.selectedItem=position;
                adapter.notifyDataSetChanged();
               /* SignalerProbleme frag = new SignalerProbleme();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, frag);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();*/
            }

        });

        prise_en_charge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                {
                    try {

                        Voyageur fragg = new Voyageur();
                        Bundle bundle=new Bundle();

                        bundle.putString("data",myarray.get(adapter.selectedItem).toString());
                        bundle.putString("fromExpress","no");
                        bundle.putString("notif","no");

                        fragg.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, fragg).commit();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        probleme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                {
                    SignalerProbleme fragment=new SignalerProbleme();
                    Bundle b=new Bundle();

                    try {
                        b.putString("offre_id",((JSONObject)myarray.get(adapter.selectedItem)).getString("order_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    fragment.setArguments(b);
                    FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment).commit();

                }
            }
        });

    }

    public void onResume() {
        Log.e("DEBUG", "onResume of HomeFragment");
        getPlanning();
        super.onResume();
    }

    public void getPlanning(){


        SharedPreferences preferences=getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String str = preferences.getString("user", "");
        RequestParams params = new RequestParams();
        JSONObject user_json = null;
        try {
            user_json = new JSONObject(str);
            params.put("user_id",user_json.getString("id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "", "Loading", true);

        VTCRestClient.postLogin(getActivity(),"VTCMyReservation", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                progressDialog.dismiss();
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("555555555555", s);

                    try {
                        adapter=new ReservationsAdapter(getActivity(),new JSONArray());

                        JSONObject myResult= new JSONObject(s);

                        Object aObj = myResult.get("result");
                        if(aObj instanceof String){

                            adapter.myReservations=new JSONArray();
                            adapter.notifyDataSetChanged();
                        }
                        if(aObj instanceof JSONArray){

                            adapter.myReservations=myResult.getJSONArray("result");
                            adapter.notifyDataSetChanged();

                        }




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    MesReservations frag = new MesReservations();


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                if (statusCode==401)
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Attention")
                            .setMessage("Votre session a expiré, veuillez reconnecter")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing

                                    Intent i=new Intent(getActivity(),LoginActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                else
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Echec")
                            .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
            }

        });

    }

}