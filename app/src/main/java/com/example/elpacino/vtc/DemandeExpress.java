package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by elpacino on 08/11/16.
 */
public class DemandeExpress extends Fragment {

    private DonutProgress donutprogress;
    private Timer timer;
    JSONObject dataobj;
    CountDownTimer mCountDownTimer;
    JSONObject userjson;
    int i=0;
    LocationManager locationManager;
    String lat,lng;
    JSONObject newpos = new JSONObject();
    ProgressDialog progressDialog;
    String timeClicked = "5";




    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.reserv_express, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState)  {     // Defines the xml file for the fragment


        Button btn5=(Button)view.findViewById(R.id.min_5);
        Button btn10=(Button)view.findViewById(R.id.min_10);
        Button btn15=(Button)view.findViewById(R.id.min_15);
        Button btn20=(Button)view.findViewById(R.id.min_20);

        TextView username=(TextView)view.findViewById(R.id.username);
        TextView adresse=(TextView)view.findViewById(R.id.address);
        TextView adresse2=(TextView)view.findViewById(R.id.address2);

        SharedPreferences preferences =getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String str = preferences.getString("user", "");
        try {

           userjson = new JSONObject(str);

        }catch (JSONException e) {
            e.printStackTrace();
        }

        final Bundle bundle = getArguments();
        if (bundle!=null) {
            String demandeReservation = bundle.getString("notif_data", "default");
            Log.i("messageVTC", "DemandeReservation.notif_data :" + demandeReservation);
            try {
                dataobj = new JSONObject(demandeReservation);

                Log.i("dataobject",dataobj.toString());

              //  endroit.setText(dataobj.getString("firstname") + " " + dataobj.getString("lastname"));
                username.setText(dataobj.getString("firstname")+" "+dataobj.getString("lastname"));
                adresse.setText(dataobj.getString("adressedep"));
//                adresse2.setText(dataobj.getString("pays")+" "+dataobj.getString("ville")+" "+dataobj.getString("zip"));




                //String time = dataobj.getString("datedep").substring(dataobj.getString("datedep").lastIndexOf(" ") + 1);
                //recep_code.setText(time);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("messageVTC","btn5 clicked");
                progressDialog = ProgressDialog.show(DemandeExpress.this.getActivity(), "Localisation en cours", "", true);
                timeClicked = "5";
               // sendreptoServer(timeClicked,true, 1);
                //getposition(DemandeExpress.this.getActivity());
                try {

                    newpos = MySingleton.getInstance().getPosition();

                    Log.i("fromsingleton", "&&&&&&&"+newpos);
                    calculateDistanceadresses(dataobj.getString("latitude"), dataobj.getString("longitude"),newpos.getDouble("latitude"), newpos.getDouble("longitude"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
        btn10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("messageVTC","btn10 clicked");
                progressDialog = ProgressDialog.show(DemandeExpress.this.getActivity(), "Localisation en cours", "", true);
                timeClicked = "10";
                //sendreptoServer(timeClicked,true, 1);
                try {

                    newpos = MySingleton.getInstance().getPosition();
                    calculateDistanceadresses(dataobj.getString("latitude"), dataobj.getString("longitude"),newpos.getDouble("latitude"), newpos.getDouble("longitude"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        btn15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("messageVTC","btn15 clicked");
                progressDialog = ProgressDialog.show(DemandeExpress.this.getActivity(), "Localisation en cours", "", true);

                timeClicked = "15";
               // sendreptoServer(timeClicked,true, 1);
                try {

                    newpos = MySingleton.getInstance().getPosition();
                    calculateDistanceadresses(dataobj.getString("latitude"), dataobj.getString("longitude"),newpos.getDouble("latitude"), newpos.getDouble("longitude"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        btn20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("messageVTC","btn20 clicked");
                progressDialog = ProgressDialog.show(DemandeExpress.this.getActivity(), "Localisation en cours", "", true);

                timeClicked = "20";
                //sendreptoServer(timeClicked,true, 1);
                try {

                    newpos = MySingleton.getInstance().getPosition();
                    calculateDistanceadresses(dataobj.getString("latitude"), dataobj.getString("longitude"),newpos.getDouble("latitude"), newpos.getDouble("longitude"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


        final ProgressBar mProgressBar = (ProgressBar)view.findViewById(R.id.progressBar);

        final TextView count=(TextView)view.findViewById(R.id.count);
        mProgressBar.setProgress(i);

        Log.i("FromNotif",""+MainActivity.getInstance().FromNotif);
        if (MainActivity.getInstance().FromNotif){
            Date now=new Date();
            Log.i("notifDate",""+MainActivity.getInstance().notifDate.getTime());
            Log.i("now date",""+now.getTime());
            long diff= now.getTime()-MainActivity.getInstance().notifDate.getTime();
            Log.i("diff",""+diff);
            if (diff>15000){
                sendreptoServer("-1",false, 0, "-1");
            }else
                mCountDownTimer=new CountDownTimer(15000-diff,1000) {

                    @Override
                    public void onTick(long millisUntilFinished) {
                        Log.v("Log_tag", "Tick of Progress"+ i+ millisUntilFinished);

                        i++;
                        count.setText(i+" s");
                        mProgressBar.setProgress(i);
                    }

                    @Override
                    public void onFinish() {
                        //Do what you want
                        i++;
                        count.setText(i+" s");
                        mProgressBar.setProgress(i);
                        sendreptoServer("-1",false, 0, "-1");
                        Log.i("count down did finish","yes");
                    }
                };
                if(mCountDownTimer != null)
                    mCountDownTimer.start();
        }else{
            mCountDownTimer=new CountDownTimer(15000,1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    Log.v("Log_tag", "Tick of Progress"+ i+ millisUntilFinished);

                    i++;
                    count.setText(i+" s");
                    mProgressBar.setProgress(i);
                }

                @Override
                public void onFinish() {
                    //Do what you want
                    i++;
                    count.setText(i+" s");
                    mProgressBar.setProgress(i);
                    sendreptoServer("-1",false, 0, "-1");
                    Log.i("count down did finish","yes");
                }
            };
            mCountDownTimer.start();
        }





        /*   timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(15 == donutprogress.getProgress()){
                            timer.cancel();

                            sendreptoServer("-1",false);
                            // refuseReservation();
                            //finish();

                        }else
                            donutprogress.setProgress(donutprogress.getProgress() + 1);
                    }
                });
            }
        }, 1000, 500);*/
    }

    public  void  sendreptoServer(String min, final Boolean accept, int credib, String credib_time){

        if (mCountDownTimer!=null)
            mCountDownTimer.cancel();

        if(getActivity() != null)
            progressDialog = ProgressDialog.show(getActivity(), "Envoi des informations", "", true);

        RequestParams params = new RequestParams();
        try {

            params.put("user_id", userjson.getString("id").toString());
            params.put("order_id", dataobj.getString("order_id"));
            params.put("delais", min);
            params.put("credibilite", credib);
            params.put("cred_time", credib_time);
            Log.i("params",params.toString());

        }catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("messageVTC","preparation de l'envoie ...");

        VTCRestClient.postLogin(getActivity(),"VTCReponse", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String s = null;
                try {
                    if (progressDialog != null)
                         progressDialog.dismiss();
                    s = new String(responseBody, "UTF-8");
                    Log.i("messageVTC","Message from Server "+s);

                    JSONObject obj = new JSONObject(s);
                    String retour=obj.getString("result");

                    if (retour instanceof String){

                        if (retour.equals("success")){
                            showAlert(accept);

                        }else  if (retour.equals("failed")){
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Echec")
                                    .setMessage("contact l'administrateur")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            AccueilActivity frag = new AccueilActivity();
                                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                            fragmentTransaction.replace(R.id.container, frag);
                                            // fragmentTransaction.addToBackStack(null);
                                            fragmentTransaction.commit(); // do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();

                        }
                        else  if (retour.equals("no order found")){
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Echec")
                                    .setMessage("Reservation annuler")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            AccueilActivity frag = new AccueilActivity();
                                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                            fragmentTransaction.replace(R.id.container, frag);
                                            // fragmentTransaction.addToBackStack(null);
                                            fragmentTransaction.commit(); // do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();

                        }
                        else{
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Echec")
                                    .setMessage("contact l'administrateur")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            getActivity().getFragmentManager().popBackStack(); // do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }

                    }else {

                        new AlertDialog.Builder(getActivity())
                                .setTitle("Echec")
                                .setMessage("dataProblem")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        AccueilActivity frag = new AccueilActivity();
                                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.container, frag);
                                        // fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();// do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                Log.i("error",error.toString());
                Log.i("status code",""+statusCode);
                if (progressDialog != null)
                     progressDialog.dismiss();

                if (statusCode==401)
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Attention")
                            .setMessage("Votre session a expiré, veuillez reconnecter")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing

                                    Intent i=new Intent(getActivity(),LoginActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                else
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Echec")
                            .setMessage("Erreur serveur.")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
            }
        });

    }

    public void showAlert(Boolean b){
        String message;
        if (b)
            message="Votre réponse a été envoyée.";
        else
            message="Vous n'avez pas répondu à demande de réservation";


        new AlertDialog.Builder(MySingleton.getInstance().myMain)
                .setTitle("Info")
                .setMessage(message)
                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                       if( MySingleton.getInstance().myMain.getFragmentManager()!=null)
                           if (mCountDownTimer!=null)
                                 mCountDownTimer.cancel();
                        AccueilActivity frag = new AccueilActivity();
                        if(getFragmentManager() != null){
                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.container, frag);
                            // fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();// do nothing
                        }

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }


    void calculateDistanceadresses(final String lat1, final String lng1, final double lat2, final double lng2){

        Log.i("expresssss", "111111");


        if (mCountDownTimer!=null)
            mCountDownTimer.cancel();

        //String addressURL = "https://maps.google.com/maps/api/directions/json?origin="+adr1+"&destination="+lat2+","+lng2+"&sensor=false&units=metric&key=AIzaSyBmJiKnqP5XlIogPz8zeaGp4Zccpf8O_No";
        //String addressURL = "https://www.waze.com/RoutingManager/routingRequest?from=x%3A-73.9922319+y%3A40.7379049&to=x%3A-73.9451895+y%3A40.8134374&at=0&returnJSON=true&returnGeometries=true&returnInstructions=true&timeout=60000&nPaths=3&options=AVOID_TRAILS%3At";
        // https://www.waze.com/RoutingManager/routingRequest?from=x%3A-73.96537017822266+y%3A40.77473068237305&to=x%3A-73.99018859863281+y%3A40.751678466796875&at=0&returnJSON=true&returnGeometries=true&returnInstructions=true&timeout=60000&nPaths=3&options=AVOID_TRAILS%3At
        // String addressURL = "https://www.waze.com/RoutingManager/routingRequest?from=x%3A48.8935204+y%3A2.379638200000045&to=x%3A48.821590423583984+y%3A2.333850383758545&at=0&returnXML=true&returnGeometries=true&returnInstructions=true&timeout=60000&nPaths=1&clientVersion=4.0.0&options=AVOID_TRAILS%3At%2CALLOW_UTURNS%3At";
        //String addressURL = "https://www.waze.com/SearchServer/mozi?pd_title=DestinationName&pd_ll=32.0835,34.7659&pd_time=1496928900";
        //String addressURL = "https://www.waze.com/row-RoutingManager/routingRequest?from=x:4.4024643+y:51.2194475&to=x:4.3517103+y:50.8503396&at=0&returnJSON=true&returnGeometries=false&returnInstructions=false&timeout=60000&nPaths=1&clientVersion=4.0.0&options=AVOID_TRAILS:t,ALLOW_UTURNS:t";
        // String addressURL = "https://www.waze.com/RoutingManager/routingRequest?from=x:-74.85216+y:40.13963+bd:true&to=x:-75.15582+y:40.1932+bd:Atrue&returnJSON=true&returnGeometries=true&returnInstructions=true&timeout=60000&nPaths=1";
        // String addressURL = "https://route.cit.api.here.com/routing/7.2/calculateroute.json?waypoint0="+lat1+"%2C"+lng1+"&waypoint1="+lat2+"%2C"+lng2+"&mode=fastest%3Bcar%3Btraffic%3Aenabled&app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg&departure=now";

        String addressURL = "http://office-hc.fr/waze.php?depart="+lat1+","+lng1+"&destination="+lat2+","+lng2;
        //String addressURL = "http://office-hc.fr/waze.php?depart="+lat1+","+lng1+"&destination=45.769248962402344,4.851263999938965";

        Log.i("adresse222",addressURL);

        Log.i("calculateDistanceadre", "11111111");

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(20000);
        client.get(addressURL, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (progressDialog != null)
                    progressDialog.dismiss();

                try {

                    Log.i("success","yes");
                    String s = new String(responseBody, "UTF-8");
                     Log.i("result String",s.toString());
                    JSONArray result= new JSONArray(s);
                    Log.i("wazeresult",result.toString());


//                    if (result.getJSONArray("routes").length()==0) {
                    if (result.length() ==0) {


                        new AlertDialog.Builder(getActivity())
                                .setTitle("Echec!")
                                .setMessage("Aucun chemin proposé, veuillez réessayer avec une autre adresse.")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        if( MySingleton.getInstance().myMain.getFragmentManager()!=null)
                                            if (mCountDownTimer!=null)
                                                mCountDownTimer.cancel();
                                        AccueilActivity frag = new AccueilActivity();
                                        if(getFragmentManager() != null){
                                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                            fragmentTransaction.replace(R.id.container, frag);
                                            // fragmentTransaction.addToBackStack(null);
                                            fragmentTransaction.commit();// do nothing
                                        }
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .show();
                    } else{





                        Double res = (Double) result.get(0);

                        int time = Integer.parseInt(timeClicked) * 60;

                        if((time + 3600) <= (res * 60) ){
                            sendreptoServer(timeClicked,true, 1, res.toString());
                        }else
                            sendreptoServer(timeClicked,true, 1,res.toString());


                    }

                } catch (UnsupportedEncodingException e) {

                    if (progressDialog != null)
                        progressDialog.dismiss();

                    new AlertDialog.Builder(DemandeExpress.this.getActivity())
                            .setTitle("Echec")
                            .setMessage("Une erreur s'est produite, waze n'as pas trouver un route. Voulez vous ressayer")
                            .setPositiveButton("Try again", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    progressDialog = ProgressDialog.show(DemandeExpress.this.getActivity(), "Waiting to get a route", "", true);

                                    calculateDistanceadresses(lat1, lng1, lat2, lng2);
                                }

                            })
                            .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if( MySingleton.getInstance().myMain.getFragmentManager()!=null)
                                        if (mCountDownTimer!=null)
                                            mCountDownTimer.cancel();
                                    AccueilActivity frag = new AccueilActivity();
                                    if(getFragmentManager() != null){
                                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.container, frag);
                                        // fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();// do nothing
                                    }
                                }
                            })

                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                    e.printStackTrace();
                } catch (JSONException e) {

                    if (progressDialog != null)
                        progressDialog.dismiss();

                    new AlertDialog.Builder(DemandeExpress.this.getActivity())
                            .setTitle("Echec")
                            .setMessage("Une erreur s'est produite, waze n'as pas trouver un route.  Voulez vous ressayer ")

                            .setPositiveButton("Try again", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    progressDialog = ProgressDialog.show(DemandeExpress.this.getActivity(), "Waiting to get a route", "", true);

                                    calculateDistanceadresses(lat1, lng1, lat2, lng2);
                                }

                            })
                            .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                     if( MySingleton.getInstance().myMain.getFragmentManager()!=null)
                                        if (mCountDownTimer!=null)
                                            mCountDownTimer.cancel();
                                    AccueilActivity frag = new AccueilActivity();
                                    if(getFragmentManager() != null){
                                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.container, frag);
                                        // fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();// do nothing
                                    }
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
            {

                if (progressDialog != null)
                    progressDialog.dismiss();

                if ( error.getCause() instanceof ConnectTimeoutException)
                {
                    new AlertDialog.Builder(getActivity())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Echec")
                            .setMessage("We can't find a route. Do you wanna try again?")
                            .setPositiveButton("Try again", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    progressDialog = ProgressDialog.show(DemandeExpress.this.getActivity(), "Waiting to get a route", "", true);

                                    calculateDistanceadresses(lat1, lng1, lat2, lng2);
                                }

                            })
                            .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();

                }


                Log.i("failure","yes  => "+statusCode);
                error.printStackTrace(System.out);
            }
        });


    }



}