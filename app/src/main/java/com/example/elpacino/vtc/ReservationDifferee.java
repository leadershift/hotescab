package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by elpacino on 22/08/16.
 */
public class ReservationDifferee extends Fragment{


    JSONObject dataobj;
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.reserv_differe, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState)  {     // Defines the xml file for the fragment


        TextView adr1= (TextView)view.findViewById(R.id.adr1);
        TextView adr2= (TextView)view.findViewById(R.id.adr2);

        TextView date1=(TextView)view.findViewById(R.id.date1);
        TextView date2=(TextView)view.findViewById(R.id.date2);
        Button accepter=(Button)view.findViewById(R.id.accept);

        final Bundle bundle = getArguments();
        if (bundle!=null) {
            String demandeReservation = bundle.getString("notif_data", "default");
            Log.i("messageVTC", "DemandeReservation.notif_data :" + demandeReservation);
            try {
                dataobj = new JSONObject(demandeReservation);

                adr1.setText(dataobj.getString("adressedep"));
                adr2.setText(dataobj.getString("adressearr"));
               // code1.setText(dataobj.getString("zip") + " " + dataobj.getString("ville"));
                //code2.setText(dataobj.getString("zip2") + " " + dataobj.getString("ville2"));


                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date datee = null;
                try {
                    Date datee11= format.parse(dataobj.getString("datedep"));
                    Date datee22= format.parse(dataobj.getString("datedarr"));
                    date1.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(datee11));
                    date2.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(datee22));
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        accepter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("messageVTC","btn5 clicked");
                sendreptoServer();
            }
        });

    }

    public  void  sendreptoServer(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        SharedPreferences preferences =getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String str = preferences.getString("user", "");
        try {
            JSONObject json = new JSONObject(str);
            params.put("user_id", json.getString("id").toString());
            params.put("order_id", dataobj.getString("order_id"));
            params.put("delais", 0);
            params.put("credibilite", 1);
            params.put("cred_time", "1");

            Log.i("params",params.toString());

        }catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("messageVTC","preparation de l'envoie ...");
        VTCRestClient.postLogin(getActivity(),"VTCReponse", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("messageVTC","Message from Server "+s);

                    JSONObject obj = new JSONObject(s);
                    String retour=obj.getString("result");

                    if (retour instanceof String){

                        if (retour.equals("success")){
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Sucess")
                                    .setMessage("Merci")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            AccueilActivity frag = new AccueilActivity();
                                            FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                                            fragmentTransaction.replace(R.id.container, frag);
                                            // fragmentTransaction.addToBackStack(null);
                                            fragmentTransaction.commit();// do nothing // do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_info)
                                    .show();

                        }else  if (retour.equals("failed")){
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Echec")
                                    .setMessage("contact l'administrateur")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            AccueilActivity frag = new AccueilActivity();
                                            FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                                            fragmentTransaction.replace(R.id.container, frag);
                                            // fragmentTransaction.addToBackStack(null);
                                            fragmentTransaction.commit();// do nothing// do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();

                        }else  if (retour.equals("no order found")){
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Echec")
                                    .setMessage("Reservation annuler")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            AccueilActivity frag = new AccueilActivity();
                                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                            fragmentTransaction.replace(R.id.container, frag);
                                            // fragmentTransaction.addToBackStack(null);
                                            fragmentTransaction.commit(); // do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();

                        }else{
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Echec")
                                    .setMessage("contact l'administrateur")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            AccueilActivity frag = new AccueilActivity();
                                            FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                                            fragmentTransaction.replace(R.id.container, frag);
                                            // fragmentTransaction.addToBackStack(null);
                                            fragmentTransaction.commit();// do nothing // do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }

                    }else {

                        new AlertDialog.Builder(getActivity())
                                .setTitle("Echec")
                                .setMessage("dataProblem")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        getActivity().getFragmentManager().popBackStack();// do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                Log.i("error",error.toString());
                Log.i("status code",""+statusCode);

                if (statusCode==401)
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Attention")
                            .setMessage("Votre session a expiré, veuillez reconnecter")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing

                                    Intent i=new Intent(getActivity(),LoginActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                else
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Echec")
                            .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
            }
        });

    }
}