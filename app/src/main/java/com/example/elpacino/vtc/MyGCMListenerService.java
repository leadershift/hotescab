package com.example.elpacino.vtc;

import android.app.ActivityManager;
import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by NgocTri on 4/9/2016.
 */
public class MyGCMListenerService extends com.google.android.gms.gcm.GcmListenerService {

    Intent intent,localIntent;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {

        Log.i("onMessageReceived ","");
        String message = data.getString("message");
        String offre = data.getString("offre");
        String type = data.getString("mtype");
        Log.i("myType",type);
        Log.i("data",data.toString());

        Log.d("MyGcmListenerService", "From: " + from);
        Log.d("MyGcmListenerService", "Message: " + message);
/*
        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }
*/
        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        sendNotification(message,offre, type);
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */

    private void sendNotification(final String message, String offre, String type) {
        Log.i("send notification 0",""+message);

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            if(r != null)r.play();
            Log.i("afterchnangefrag" ,"222222");
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (type.equals("7")){
            try {
                JSONObject obj = new JSONObject(offre);
                Log.i("offre",obj.toString());
                        if (MainActivity.getInstance()!=null) {
                            Log.i("messageVTC", "exist11111");
                            if (MainActivity.getInstance().isinFG) {

                                Log.i("messageVTC", "exist");
                                MainActivity activity = MainActivity.getInstance();
                                activity.refreshifannuled();


                            }
                        }
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle("Hotel Cabs")
                    .setContentText(obj.getString("firstname")+" "+obj.getString("lastname")+" a annulé sa réservation")
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, notificationBuilder.build());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {

            Intent intent= new Intent(this, MainActivity.class);
            String fragment=null;

            if (type.equals("1")){
                fragment="DemandeReservation";
               // intent.putExtra("fragment", "DemandeReservation");
            }else if (type.equals("2")){
                fragment="ReservationDifferee";
              //  intent.putExtra("fragment", "ReservationDifferee");
            }else if (type.equals("3")) {
                fragment="ReservationConfirm";
              //  intent.putExtra("fragment", "ReservationConfirm");
            }else if (type.equals("5")) {
                fragment="DemandeExpress";
              //  intent.putExtra("fragment", "DemandeExpress");
                Log.i("Express","");
            }

            SharedPreferences preferences =getSharedPreferences("pref", Context.MODE_PRIVATE);
            boolean exist = preferences.getBoolean("Main_active",false);
            if (MainActivity.getInstance()!=null){
                if (MainActivity.getInstance().isinFG){

                    Log.i("messageVTC","exist");
                    MainActivity activity =MainActivity.getInstance();
                    activity.changefragment1(offre,type);

                    Log.i("afterchnangefrag" ,"1111111111");

                }else{
                    Log.i("type",""+type);
                    Log.i("messageVTC","non exist");
                    intent.putExtra("from_notif", "yes");
                    intent.putExtra("notif_data", offre);
                   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                   // intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    Log.i("--------","***************");
                    Log.i("notif_data",""+offre);
                    Log.i("*-*--*-*-*-*-*-",""+intent.getStringExtra("from_notif"));
                    intent.setAction("foo");
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,0);

                    MainActivity.getInstance().SetIntentExtras(offre,fragment);

                    Uri defaultSoundUri =RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);;
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle("Hotels Cab")
                            .setContentText(message)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);

                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager.notify(0, notificationBuilder.build());
                }
            }else{
                Log.i("messageVTC111111","non exist");
                intent.putExtra("from_notif", "yes");
                intent.putExtra("notif_data", offre);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Log.i("notif_data",""+offre);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri =RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);;
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Hotels Cab")
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(0, notificationBuilder.build());
            }
        }

        //Intent intent = new Intent(this, MainActivity.class);
      //  Intent intent = MainActivity.getInstance();
    }
}
