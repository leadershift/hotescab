package com.example.elpacino.vtc;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    NavigationView navigationView;
    ImageButton menu;
    Toolbar toolbar;
    Fragment myfragment,frag;
    Intent intent;
    ProgressDialog progressDialog;
    JSONObject user,course;
    JSONObject PlanningResult;
    MediaPlayer mp;

    private static MainActivity sInstance;

    public MainActivity() {
    }

    public static MainActivity getInstance(){
        return sInstance;
    }
    public static boolean isinFG = false;
    public static boolean FromNotif = false;
        String data;
        String fragment;
    public Date notifDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        sInstance = this;
        MySingleton.getInstance().myMain=this;

        setContentView(R.layout.activity_home);


       /* try {
            mp = new MediaPlayer();
            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mp.setDataSource("http://mali_election.app.tn/uploads/reclamation/5d2a39a94a892b04eaf681443d4004e0.3gp");
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            // handle exception
            Log.i("exeption","ZZZZ");
        }*/

        Bundle extras = getIntent().getExtras();
        Log.i("0000000000",""+extras);

        SharedPreferences preferences =getSharedPreferences("pref", Context.MODE_PRIVATE);
        String str = preferences.getString("user", "");


        try {
            user=new JSONObject(str);
           // MySingleton.getInstance().getPlanning(user.getString("id").toString(),this);



        } catch (JSONException e) {
            e.printStackTrace();
        }



        menu = (ImageButton) findViewById(R.id.menu);

        ImageButton compte = (ImageButton) findViewById(R.id.compte);


        AccueilActivity frag = new AccueilActivity();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, frag);
       // fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        compte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CompteActivity frag = new CompteActivity();
               /* Bundle bundle = new Bundle();
                try {
                    bundle.putString("reservations", PlanningResult.getString("result").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                frag.setArguments(bundle);*/
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, frag);
              //  fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


        setNavigationDrawer();
    }
    public  void changefragment1(final String notif_data, String type){

        FromNotif = false;
        Log.i("messageVTC","changefragment1");

        Log.i("type",""+type);
        Log.i("messageVTC",notif_data);

        Fragment fff=null;
        final Bundle bundle=new Bundle();
        if (type.equals("1"))
            fff = new DemandeReservation();
        else if (type.equals("2"))
            fff = new ReservationDifferee();
        else if (type.equals("3"))
            fff = new ReservationConfirm();
        else if (type.equals("5")) {
            Log.i("Express","");
            fff = new DemandeExpress();
        }else{
            if (type.equals("4")){
               final Fragment frag= new PriseEnCharge();

                Timer myTimer = new Timer();
                myTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        this.cancel();
                        bundle.putString("notif_data", notif_data);
                        frag.setArguments(bundle);
                        FragmentTransaction fragmentTransaction =getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, frag).commit();
                    }

                },10000);

            }
        }

        if (!type.equals("4")) {
            bundle.putString("notif_data", notif_data);
            fff.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fff).commit();
            Log.i("didStartActivity", "yes");

            Log.i("buundle", ""+fff);

        }
    }

    public void refreshifannuled(){


      /*  AccueilActivity frag=new AccueilActivity();
        Bundle b=new Bundle();
        //b.putString("reservations", myResult.getString("result").toString());
        frag.setArguments(b);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.container, frag);
        //    transaction.addToBackStack(null);
        transaction.commit();*/

         if(MySingleton.getInstance().getTimer() != null){
             MySingleton.getInstance().getTimer().cancel();
         }


        Fragment currentFragment = MainActivity.this.getFragmentManager().findFragmentById(R.id.container);
        if (currentFragment instanceof AccueilActivity) {
            //do your stuff here
            //getPlanning();

            Log.i("qsdqsqsd","aceuiiiiiill");
            AccueilActivity frag=new AccueilActivity();
            Bundle b=new Bundle();
            //b.putString("reservations", myResult.getString("result").toString());
            frag.setArguments(b);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            transaction.replace(R.id.container, frag);
            //    transaction.addToBackStack(null);
            transaction.commit();



        }else if(currentFragment instanceof MesReservations) {
            //do your stuff here
            //getPlanning();

            Log.i("qsdqsqsd","MesReservations");
            MesReservations frag=new MesReservations();

            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            transaction.replace(R.id.container, frag);
            //    transaction.addToBackStack(null);
            transaction.commit();



        }

    }

    void  loadData(JSONObject object){

        PlanningResult=object;
        try {

            AccueilActivity frag = new AccueilActivity();
            Bundle myB=new Bundle();
            myB.putString("reservations", PlanningResult.getString("result").toString());
            frag.setArguments(myB);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, frag);
          //  fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(FromNotif){

            Log.i("11111111111111111","222222222222222222");
            Fragment frag = null;
            if (fragment.equals("DemandeReservation"))
                frag=new DemandeReservation();
            else if (fragment.equals("ReservationDifferee"))
                frag=new ReservationDifferee();
            else if (fragment.equals("ReservationConfirm"))
                frag=new ReservationConfirm();
            else if (fragment.equals("DemandeExpress"))
                frag=new DemandeExpress();
            else
                frag=new PriseEnCharge();

            Bundle b = new Bundle();
            b.putString("from_notif", "yes");
            b.putString("notif_data", data);
            frag.setArguments(b);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, frag);
            fragmentTransaction.commit();

           // FromNotif=false;
        }

        // Store our shared preference
        SharedPreferences sp = getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putBoolean("Main_active", true);
        ed.commit();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Store our shared preference
        SharedPreferences sp = getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putBoolean("Main_active", false);
        ed.commit();

    }

    public void onPause() {
        super.onPause();
        isinFG = false;

    }

    public void onResume() {
        super.onResume();
        isinFG = true;

    }

    public void SetIntentExtras(final String adata, final String afragment){

        FromNotif =true;
        data=adata;
        fragment=afragment;

        if (fragment.equals("DemandeReservation") || fragment.equals("DemandeExpress"))
            notifDate=new Date();


    }

    public void onDestroy() {
        super.onDestroy();
        isinFG = false;
    }

    public void setNavigationDrawer() {

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }


                SharedPreferences preferences =getSharedPreferences("pref", Context.MODE_PRIVATE);

                NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
                Menu menu1 = nav.getMenu();


                MenuItem encourt = (MenuItem) menu1.findItem(R.id.course_encour);

                if(preferences.contains("course")){
                    String courseencourt = preferences.getString("course", "");
                    try {
                        course = new JSONObject(courseencourt);
                        encourt.setVisible(true);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    encourt.setVisible(false);
                }

            }
        });

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setVerticalFadingEdgeEnabled(true);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        View header=navigationView.getHeaderView(0);
        TextView name = (TextView)header.findViewById(R.id.user_name);
        TextView email = (TextView)header.findViewById(R.id.email);

        try {

            name.setText("Bonjour "+user.getString("firstname"));
            email.setText(user.getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        myfragment = null;
        JSONObject json=null;

        int id = item.getItemId();

            if (id == R.id.nav_accueil) {
                myfragment = new AccueilActivity();

            }else if (id == R.id.nav_compte) {

                myfragment = new CompteActivity();

            }else if (id == R.id.nav_reserv) {

              getPlanning();

            }else if (id == R.id.course_encour) {
                Log.i("course_encour", "111111111111111");
             /////////////////////////////////
                GuidageGPS fragment=new GuidageGPS();
                Bundle b=new Bundle();

                Log.i("hashmap",course.toString());
                b.putString("adr_data",course.toString());

                fragment.setArguments(b);
                FragmentTransaction fragmentTransaction = MainActivity.this.getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment).commit();



            }else if (id== R.id.logout){
                SharedPreferences mySPrefs = getSharedPreferences("pref",MODE_PRIVATE);
                SharedPreferences.Editor editor = mySPrefs.edit();
                editor.remove("user");
                editor.commit();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }

        if (myfragment != null) {

            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            transaction.replace(R.id.container, myfragment);
        //    transaction.addToBackStack(null);
            transaction.commit();
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawers();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void getPlanning(){

        SharedPreferences preferences=getSharedPreferences("pref", Context.MODE_PRIVATE);
        String str = preferences.getString("user", "");
        try {
            JSONObject user_json = new JSONObject(str);
            RequestParams params = new RequestParams();
            params.put("user_id",user_json.getString("id"));

            final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this, "", "Loading", true);

            VTCRestClient.postLogin(MainActivity.this,"VTCMyReservation", params, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                    progressDialog.dismiss();
                    String s = null;
                    try {
                        s = new String(responseBody, "UTF-8");
                        Log.i("Message from Server", s);

                        try {
                            JSONObject myResult= new JSONObject(s);

                            MesReservations frag=new MesReservations();
                            Bundle b=new Bundle();
                            b.putString("reservations", myResult.getString("result").toString());
                            frag.setArguments(b);
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();

                            transaction.replace(R.id.container, frag);
                        //    transaction.addToBackStack(null);
                            transaction.commit();
                            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                            drawer.closeDrawers();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                    progressDialog.dismiss();
                    if (statusCode==401)
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Attention")
                                .setMessage("Votre session a expiré, veuillez reconnecter")
                                .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing

                                        Intent i=new Intent(MainActivity.this,LoginActivity.class);
                                        startActivity(i);
                                        finish();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    else
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Echec")
                                .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                                .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                }

            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}