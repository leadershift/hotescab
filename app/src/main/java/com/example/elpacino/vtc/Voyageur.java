package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;
import java.util.logging.LogRecord;


import cz.msebera.android.httpclient.Header;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by elpacino on 23/08/16.
 */
public class Voyageur extends Fragment {

    JSONObject dataobj;
    String dest,depart;
    float distance=0,prixorig;
    Float lat1,lng1,lat2,lng2;
    TextView prix;
    Button demarrer, modifier;
    String notif;
    Boolean start = false;
    Timer t;
    TextView adr;
    Long diff;
    JSONObject depart_coordinates,dest_coordinates;
    ProgressDialog progressDialog;
    public int seconds = 00;
    public int secondsM = 00;
    public int minutesM = 00;
    public int minutes = 00;
    public int totalTime = 00;
    String sec,min,secM,minM;
    Bundle bundle1;

    private TextView timeview;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.voyageur, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState)  {     // Defines the xml file for the fragment


        TextView nom = (TextView)view. findViewById(R.id.nom);
        TextView phone = (TextView)view. findViewById(R.id.phone);
        adr = (TextView)view. findViewById(R.id.zone);
        TextView date = (TextView)view. findViewById(R.id.arrivee);
        TextView recep_code = (TextView)view. findViewById(R.id.recep_code);
        prix = (TextView) view.findViewById(R.id.prix);
        demarrer = (Button) view.findViewById(R.id.demarrer);
        modifier = (Button) view.findViewById(R.id.modifier);
        Button probleme=(Button)view.findViewById(R.id.probleme);

      //  modifier.setBackgroundResource(R.drawable.editt);

        timeview = (TextView)view. findViewById(R.id.timer);

         bundle1 = getArguments();

        Log.i("my_reservation",bundle1.toString());
        String dataB = bundle1.getString("data", "default");

        try {
            JSONObject dataBB = new JSONObject(dataB);

            String data_dep = dataBB.getString("date_dep2");
            Log.i("datedepart", data_dep+"");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Date datedep = format.parse(data_dep);

            Log.i("datedepart", datedep+"");

            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            String datenow = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

            Log.i("datedepartnoww", datenow+"");

            if(datedep.before(format.parse(datenow))){

                start = true;

            }else{



                //SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
                Date endDate = format.parse(data_dep);
                Log.i("endDate", "" + endDate);
                String currentTime = format.format(new Date());
                Date startDate = format.parse(currentTime);
                Log.i("startDate", "" + startDate);

                long diffInMs = endDate.getTime() - startDate.getTime();

                long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);

                diff = diffInSec;
                Log.i("diffInSec", "" + diffInSec);

            }



        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if(bundle1.containsKey("attente")){

            totalTime = Integer.parseInt(bundle1.getString("attente"));


            timeview.setText(totalTime/60+":"+ totalTime % 60);
        }


        if(!(bundle1.containsKey("frompaiment"))){
             seconds = totalTime % 60;
             minutes = totalTime/60;
            t = new Timer();
            if(start){



                if (Voyageur.this.getActivity() != null)
                {
                    t.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            if (Voyageur.this.getActivity() != null) {
                                Voyageur.this.getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //TextView timeview = (TextView) view.findViewById(R.id.time);


                                        if (seconds == 59) {
                                            //timeview.setText(String.valueOf(minutes)+":"+String.valueOf(seconds));

                                            seconds = 00;
                                            minutes = minutes + 01;

                                        } else {

                                            seconds += 01;
                                        }

                                        if (seconds < 10) {
                                            sec = "0" + String.valueOf(seconds);
                                        } else {
                                            sec = String.valueOf(seconds);
                                        }

                                        if (minutes < 10) {
                                            min = "0" + String.valueOf(minutes);
                                        } else {
                                            min = String.valueOf(minutes);
                                        }

                                        timeview.setText(min + ":" + sec);

                                        totalTime++;
                                    }
                                });
                            }
                        }

                    }, 0, 1000);

                }
            }else{

                secondsM = (int) (diff % 60);
                minutesM = (int) (diff/60);

                Log.i("minutes",diff+"");
                Log.i("minutes",minutesM+":"+secondsM);


                final Timer tmoin = new Timer();
                //TextView timeview = (TextView) view.findViewById(R.id.time);
                if (Voyageur.this.getActivity() != null)
                {
                    tmoin.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            if (Voyageur.this.getActivity() != null) {
                                Voyageur.this.getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //TextView timeview = (TextView) view.findViewById(R.id.time);




                                        if (secondsM == 00) {
                                            //timeview.setText(String.valueOf(minutes)+":"+String.valueOf(seconds));

                                            secondsM = 59;
                                            minutesM = minutesM - 01;

                                        } else {

                                            secondsM -= 01;
                                        }

                                        if (secondsM < 10) {
                                            secM = "0" + String.valueOf(secondsM);
                                        } else {
                                            secM = String.valueOf(secondsM);
                                        }

                                        if (minutesM < 10) {
                                            minM = "0" + String.valueOf(minutesM);
                                        } else {
                                            minM = String.valueOf(minutesM);
                                        }
                                        Log.i("minutes",minutesM+":"+secondsM);
                                        Log.i("minutes",minM+":"+secM);
                                        timeview.setText(minM + ":" + secM);

                                        //totalTime++;
                                    }
                                });
                            }
                        }

                    }, 0, 1000);

                }





                if (Voyageur.this.getActivity() != null)
                {
                    Timer myTimer = new Timer();
                    myTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if (Voyageur.this.getActivity() != null) {
                                Voyageur.this.getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        t.cancel();
                                        tmoin.cancel();
                                        t = new Timer();
                                        //TextView timeview = (TextView) view.findViewById(R.id.time);
                                        if (Voyageur.this.getActivity() != null)
                                        {
                                            t.scheduleAtFixedRate(new TimerTask() {
                                                @Override
                                                public void run() {
                                                    if (Voyageur.this.getActivity() != null) {
                                                        Voyageur.this.getActivity().runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                //TextView timeview = (TextView) view.findViewById(R.id.time);


                                                                Log.i("111111","122222222");

                                                                if (seconds == 59) {
                                                                    //timeview.setText(String.valueOf(minutes)+":"+String.valueOf(seconds));

                                                                    seconds = 00;
                                                                    minutes = minutes + 01;

                                                                } else {

                                                                    seconds += 01;
                                                                }

                                                                if (seconds < 10) {
                                                                    sec = "0" + String.valueOf(seconds);
                                                                } else {
                                                                    sec = String.valueOf(seconds);
                                                                }

                                                                if (minutes < 10) {
                                                                    min = "0" + String.valueOf(minutes);
                                                                } else {
                                                                    min = String.valueOf(minutes);
                                                                }

                                                                timeview.setText(min + ":" + sec);

                                                                totalTime++;
                                                            }
                                                        });
                                                    }
                                                }

                                            }, 0, 1000);

                                        }
                                    }
                                });
                            }
                        }

                    }, diff*1000);

                }
            }



        }


        SharedPreferences preferences =getActivity().getSharedPreferences("pref", MODE_PRIVATE);
        String user = preferences.getString("user", "");
        try {
            JSONObject obj=new JSONObject(user);
            recep_code.setText(obj.getString("code"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Bundle bundle = getArguments();

        if (bundle != null) {
            try {
                String demandeReservation = bundle.getString("data", "default");
                Log.i("my reservation data", demandeReservation);
                dataobj = new JSONObject(demandeReservation);

                notif=bundle.getString("notif", "default");
                if (notif.equals("yes")){

                    nom.setText(dataobj.getString("firstname")+" "+dataobj.getString("lastname"));
                    phone.setText(dataobj.getString("phone"));
                    Log.i("myyy date",dataobj.getString("datedep"));

                    if(dataobj.has("adresse_arr")){
                        dest = dataobj.getString("adresse_arr").toString();
                        adr.setText(dest);
                    }else{
                        demarrer.setText("Indiquer la destination");
                        adr.setText("--");
                    }

                    Log.i("my_reservation",dataobj.getString("datedep"));
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        Date datee = format.parse(dataobj.getString("datedep"));
                        String s = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(datee);
                        String time = s.substring(s.lastIndexOf(" ") + 1);
                        Log.i("time",time.toString());

                        date.setText("Attente départ > "+time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    depart = dataobj.getString("adressedep").toString();
                    float price= Float.parseFloat(dataobj.getString("prix"));
                    prixorig = price;

                    if( price < Float.parseFloat(dataobj.getString("minPrice")))
                            price = Float.parseFloat(dataobj.getString("minPrice"));

                    prix.setText("Prix :"+String.format("%.2f", price * 1.2)+" €");

                }else{

                    if (bundle.getString("fromExpress").equals("yes")) {

                        dest = bundle.getString("adr");
                        adr.setText(dest);
                    } else {
                        if(dataobj.has("adresse_arr")){
                            dest = dataobj.getString("adresse_arr").toString();
                            adr.setText(dest);
                        }else{
                            demarrer.setText("Indiquer la destination");
                            adr.setText("--");
                        }

                    }

                    String name = "";
                    if (dataobj.has("voyageur"))
                        name = dataobj.getJSONObject("voyageur").getString("firstname") + " " + dataobj.getJSONObject("voyageur").getString("lastname");
                    else
                        name = dataobj.getJSONObject("receptif").getString("firstname") + " " + dataobj.getJSONObject("receptif").getString("lastname");

                    nom.setText(name);

                    String phone_number = dataobj.has("voyageur") ? dataobj.getJSONObject("voyageur").getString("phone") : dataobj.getJSONObject("receptif").getString("phone");
                    phone.setText(phone_number);

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                    try {
                        Date datee = format.parse(dataobj.getString("date_dep"));
                        String s = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(datee);
                        String time = s.substring(s.lastIndexOf(" ") + 1);
                        Log.i("time",time.toString());

                        date.setText("Attente départ > "+time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    depart = dataobj.getString("adresse_dep").toString();
                    float price=Float.parseFloat(dataobj.getString("prix"));
                    prixorig = price;
                    if( price < Float.parseFloat(dataobj.getString("minPrice")))
                        price = Float.parseFloat(dataobj.getString("minPrice"));


                    prix.setText("Prix :"+String.format("%.2f", price * 1.2)+" €");
                    // getCoordinates(depart);
                }
             //   Log.i("my dest", dest);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String result=getArguments().getString("adr");
            if (result!=null){
                dest=result;

                adr.setText(result);

                demarrer.setText("Démarrer");
               float price =Float.parseFloat(getArguments().getString("price"));

                try {
                    JSONObject obj = new JSONObject(getArguments().getString("data"));
                    if( price < Float.parseFloat(obj.getString("minPrice")))
                        price = Float.parseFloat(obj.getString("minPrice"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }




                prix.setText("Prix : "+String.format("%.2f", price * 1.2)+" €");
                price =Float.parseFloat(getArguments().getString("price"));
                Log.i("priiice",getArguments().getString("price"));
                try {
                    dataobj.put("prix",getArguments().getString("price"));
                    dataobj.put("adresse_arr",result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("result",result);
                String coordinates=getArguments().getString("coordinates");
                JSONObject coor=null;
                try {
                    coor=new JSONObject(coordinates);
                    dataobj.put("latitude_arr",coor.getString("lat"));
                    dataobj.put("longitude_arr",coor.getString("lon"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.i("new Data object",dataobj.toString());

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String datee = df.format(Calendar.getInstance().getTime());

            }

            probleme.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    {
                        try {

                            if((t != null))
                                t.cancel();

                            SignalerProbleme fragg = new SignalerProbleme();
                            Bundle bundle=new Bundle();

                            bundle.putString("offre_id",dataobj.getString("order_id"));
                            bundle.putString("attente", String.valueOf(totalTime));

                            fragg.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragg).addToBackStack(null).commit();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            demarrer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                if((t != null))
                    t.cancel();

                    try {
                        Log.i("data object -----",dataobj.toString());
                    if(!dataobj.has("adresse_arr")){


                        Express fragment=new Express();
                        Bundle b=new Bundle();
                        b.putString("order_id", dataobj.getString("order_id"));
                        b.putString("latitude_dep",dataobj.getString("latitude_dep"));
                        b.putString("longitude_dep",dataobj.getString("longitude_dep"));
                        b.putString("attente", String.valueOf(totalTime));
                        b.putString("data",dataobj.toString());


                        fragment.setArguments(b);

                        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, fragment).commit();

                    }else{
                        progressDialog = ProgressDialog.show(Voyageur.this.getActivity(), "Chargement", "", true);
                        Log.i("messageVTC", "btn5 clicked");
                        Log.i("messageVTC", "btn5 clicked"+dataobj.toString());
                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String date = df.format(Calendar.getInstance().getTime());
                        BeginCourse(dataobj.getString("order_id"),dest,dataobj.getString("latitude_arr"),dataobj.getString("longitude_arr"), totalTime);



                    }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            modifier.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Express fragment=new Express();

                    Log.i("dataaaaaa",dataobj.toString().toString());
                    Bundle b=new Bundle();
                    try {
                        b.putString("order_id", dataobj.getString("order_id"));

                    b.putString("latitude_dep",dataobj.getString("latitude_dep"));
                    b.putString("longitude_dep",dataobj.getString("longitude_dep"));
                        b.putString("attente", String.valueOf(totalTime));
                    b.putString("data",dataobj.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    fragment.setArguments(b);

                    FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment).commit();
                }
            });
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("Tag", "FragmentA.onDestroyView() has been called.");
    }



    void BeginCourse(String offre_id, final String dest_adr, final String latitude, final String longitude, int timeattent){

       // Log.i("depart adress",depart_adr);

        if(MySingleton.getInstance().getTimer()!= null){
            MySingleton.getInstance().getTimer().cancel();
        }

        final AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        SharedPreferences preferences =getActivity().getSharedPreferences("pref", MODE_PRIVATE);
        String str = preferences.getString("user", "");
        try {
            JSONObject json = new JSONObject(str);
            params.put("order_id", offre_id);
            params.put("attente", timeattent);

            Log.i("attentetemp",timeattent+"");


        }catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("messageVTC","preparation de l'envoie ...");
        VTCRestClient.postLogin(getActivity(),"priseEnCharge", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String s = null;
                if (progressDialog != null)
                    progressDialog.dismiss();
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("messageVTC","Message from Server "+s);

                    JSONObject obj = new JSONObject(s);
                    String retour=obj.getString("result");


                    if (retour instanceof String){

                        if (retour.equals("success")){
                            String retour2=obj.getString("price");
                            String retour3=obj.getString("attenteprix");
                            String old=obj.getString("old");

                            Log.i("attenteprixvoyageur",retour3);


                                dataobj.put("prix",old);




                            GuidageGPS fragment=new GuidageGPS();
                            Bundle b=new Bundle();
                            obj.put("address_dest",dest_adr);
                            obj.put("lat",latitude);
                            obj.put("log",longitude);
                            obj.put("pricetotal",retour2);
                            obj.put("attenteprix",retour3);
                            obj.put("old",old);
                            obj.put("user_data",dataobj.toString());
                            Log.i("hashmap",obj.toString());
                            b.putString("adr_data",obj.toString());

                            Log.i("data111111",dataobj.toString());
                            Log.i("data111111","---------------");
                            Log.i("data111111",obj.toString());

                            SharedPreferences preferences =Voyageur.this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("course", obj.toString());
                            editor.commit();

                            Log.i("voyageuur", obj+"");
                            Log.i("voyageuur", "111111111111111");


                            fragment.setArguments(b);
                            FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment).commit();

                        }else  if (retour.equals("failed")){
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Echec")
                                    .setMessage("contact l'administrateur")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                          //  finish(); // do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();

                        }else{
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Echec")
                                    .setMessage("contact l'administrateur")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            AccueilActivity frag = new AccueilActivity();
                                            FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                                            fragmentTransaction.replace(R.id.container, frag);
                                            // fragmentTransaction.addToBackStack(null);
                                            fragmentTransaction.commit();// do nothing// do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }

                    }else {

                        new AlertDialog.Builder(getActivity())
                                .setTitle("Echec")
                                .setMessage("dataProblem")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        AccueilActivity frag = new AccueilActivity();
                                        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.container, frag);
                                        // fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();// do nothing; // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                if (progressDialog != null)
                    progressDialog.dismiss();

                if (statusCode==401)
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Attention")
                            .setMessage("Votre session a expiré, veuillez reconnecter")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing

                                    Intent i=new Intent(getActivity(),LoginActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                else
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Echec")
                            .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
            }
        });

    }

  /*  public void getCoordinates(final String depart_adr){
        String addressURL = "";


        try {
            addressURL = "http://maps.google.com/maps/api/geocode/json?address=" + URLEncoder.encode(depart_adr, "UTF-8");
            Log.i("adresse",addressURL);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(addressURL, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {

                    Log.i("success","yes");
                    String s = new String(responseBody, "UTF-8");
                    Log.i("result String",s.toString());
                    JSONObject result= new JSONObject(s);
                    Log.i("result",result.toString());
                    JSONObject js= (JSONObject) result.getJSONArray("results").get(0);
                    depart_coordinates=js.getJSONObject("geometry").getJSONObject("location");
                    lat1=Float.parseFloat(depart_coordinates.getString("lat"));
                    lng1=Float.parseFloat(depart_coordinates.getString("lng"));
                    Log.i("Coordinates",depart_coordinates.toString());

                    try {
                      String URL = "http://maps.google.com/maps/api/geocode/json?address=" + URLEncoder.encode(dest, "UTF-8");
                        Log.i("adresse",URL);

                        AsyncHttpClient client = new AsyncHttpClient();

                        client.get(URL, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                try {

                                    Log.i("success","yes");
                                    String s = new String(responseBody, "UTF-8");
                                    Log.i("result String",s.toString());
                                    JSONObject result= new JSONObject(s);
                                    Log.i("result",result.toString());
                                    JSONObject js= (JSONObject) result.getJSONArray("results").get(0);
                                    dest_coordinates=js.getJSONObject("geometry").getJSONObject("location");
                                    lat2=Float.parseFloat(dest_coordinates.getString("lat"));
                                    lng2=Float.parseFloat(dest_coordinates.getString("lng"));
                                    calculateDistance(lat1,lat2,lng1,lng2);

                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable
                                    error)
                            {

                                Log.i("failure","yes");
                                error.printStackTrace(System.out);
                            }
                        });

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable
                    error)
            {

                Log.i("failure","yes");
                error.printStackTrace(System.out);
            }
        });

    }
*/
   /* public float calculateDistance(float lat1,float lat2,float lng1,float lng2){



        String addressURL = "http://maps.google.com/maps/api/directions/json?origin=35.719763,10.574550&destination=35.717534,10.554844&sensor=false&units=metric";
        Log.i("adresse",addressURL);

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(addressURL, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {

                    Log.i("success","yes");
                    String s = new String(responseBody, "UTF-8");
                    Log.i("result String",s.toString());
                    JSONObject result= new JSONObject(s);
                    Log.i("result",result.toString());
                    JSONObject js= (JSONObject) result.getJSONArray("routes").get(0);
                    JSONObject obj= (JSONObject) js.getJSONArray("legs").get(0);
                    JSONObject distance_obj=obj.getJSONObject("distance");
                    distance=distance_obj.getInt("value");
                    float price=distance/1000*15;
                    prix.setText("Prix : "+price+"€");

                    Log.i("distance",""+distance);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable
                    error)
            {

                Log.i("failure","yes");
                error.printStackTrace(System.out);
            }
        });

        return distance;

    }*/

}