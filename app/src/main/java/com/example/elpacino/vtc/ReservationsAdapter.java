package com.example.elpacino.vtc;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by elpacino on 03/08/16.
 */
public class ReservationsAdapter extends BaseAdapter{

   public JSONArray myReservations;
    Activity act;
    int selectedItem;
    private static LayoutInflater inflater = null;

    public ReservationsAdapter(Activity a, JSONArray  list) {
        this.myReservations=list;
        act=a;
        Log.i("Mes reservations", myReservations.toString());

        inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getCount() {

        Log.i("my array count",""+myReservations.length());
        return myReservations.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        vi = inflater.inflate(R.layout.reservation_cell, null);
        TextView reserv_text=(TextView)vi.findViewById(R.id.reserv);

        if (selectedItem==position){
            vi.setBackgroundColor(Color.GRAY);
        }

        try {
            JSONObject obj=myReservations.getJSONObject(position);
            Log.i("obj",obj.toString());

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            try {
                Date datee = format.parse(obj.getString("date_dep"));
                String s=new SimpleDateFormat("dd/MM/yyyy HH:mm").format(datee);
              //  String time = s.substring(s.lastIndexOf(" ") + 1);

                String name="";
                if (obj.has("voyageur"))
                    name=obj.getJSONObject("voyageur").getString("firstname")+" "+obj.getJSONObject("voyageur").getString("lastname");
                else
                    name=obj.getJSONObject("receptif").getString("firstname")+" "+obj.getJSONObject("receptif").getString("lastname");

                reserv_text.setText(obj.getJSONObject("receptif").getString("company")+" - "+name+" - "+s+"\n"+obj.getString("adresse_dep"));
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return vi;
    }
}