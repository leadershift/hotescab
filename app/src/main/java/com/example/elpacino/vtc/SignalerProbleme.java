package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by elpacino on 03/08/16.
 */
public class SignalerProbleme extends Fragment {

    String offre_id,attente;
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.signaler_prob, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState){     // Defines the xml file for the fragment

        Button autre_probleme=(Button)view.findViewById(R.id.autre);
        final Button btn1=(Button)view.findViewById(R.id.but1);
        Button btn2=(Button)view.findViewById(R.id.but2);
        Button btn3=(Button)view.findViewById(R.id.but3);

        final Bundle bundle = getArguments();
        if (bundle != null) {
            offre_id = bundle.getString("offre_id", "default");
            attente = bundle.getString("attente", "default");
        }

        autre_probleme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProblemeSpecifique fragg = new ProblemeSpecifique();
                Bundle bundle=new Bundle();

                bundle.putString("offre_id",offre_id);

                fragg.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragg).commit();

            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonOnClick(v);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonOnClick(v);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonOnClick(v);
            }
        });





    }

    public void buttonOnClick(View view) {

        SharedPreferences preferences = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String str = preferences.getString("user", "");
        Log.i("str", str);

        Log.i("button text",((Button) view).getText().toString());


            try {
                JSONObject user_json = new JSONObject(str);
                MySingleton.getInstance().IndiquerProbleme(user_json.getString("id"),offre_id ,((Button) view).getText().toString(),this,attente);
            } catch (JSONException e) {
                e.printStackTrace();
            }


    }
}