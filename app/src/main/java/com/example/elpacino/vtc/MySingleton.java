package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.sql.StatementEvent;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by elpacino on 05/09/16.
 */
public class MySingleton {
    private static MySingleton ourInstance = new MySingleton();

    public static MySingleton getInstance() {

        return ourInstance;
    }
    Timer t;
    private String token;
    private String Access_token;
    public MainActivity myMain;
    int seconds,minutes, totalTime,isrunning=0;
    String sec, min;

    public JSONObject newpos = null;//new JSONObject();

    JSONObject myResult;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    LocationManager locationManager;

    public String getToken(){
        return token;
    }
    public void setToken(String s){
        token = s;
    }
    public void setAccessToken(String s){
        Access_token = s;
    }
    public String getAccessToken(){
         return Access_token ;
    }

    public JSONObject getPosition (){
        if(newpos == null){
            newpos = new JSONObject();

            try {
                newpos.put("longitude", "45.769248962402344");
                newpos.put("latitude", "4.851263999938965");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return newpos;
    }

    public int getSeconds(){
        return seconds;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setSeconds(int second){
        seconds = second;
    }

    public void setMinutes(int minute){
        seconds = minute;
    }

    public int getTotalTime(){
        return totalTime;
    }

    public void setIsRunning(int isrun){
        isrunning = isrun;
    }

    public  int getIsRunning(){
        return isrunning;
    }

    public void setTimer(Timer time){
        t = time;

        Log.i("silgleton","11111");
    }

    public Timer getTimer(){
        Log.i("silgleton","22222"+t);
        return t;
    }



    private MySingleton() {
    }

    void IndiquerProbleme(String user_id, final String order_id, String probl, final Fragment act, String attente){

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        params.put("user_id",user_id);
        params.put("order_id",order_id);
        params.put("problem",probl);
        params.put("attente",attente);

        Log.i("params",params.toString());

        VTCRestClient.postLogin(act.getActivity(),"problem", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("Message from Server", s);

                    JSONObject obj = new JSONObject(s);
                    if (obj.getString("result").toString().matches("success")){

                        new AlertDialog.Builder(myMain)
                                .setTitle("Succès!")
                                .setMessage("Votre problème a été indiqué avec succès.")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        AccueilActivity frag = new AccueilActivity();
                                        FragmentTransaction fragmentTransaction = myMain.getFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.container, frag);
                                        // fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();

                                        Resend(act,order_id);

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .show();

                    }else if(obj.getString("result").toString().matches("wait")) {


                        new AlertDialog.Builder(myMain)
                                .setTitle("Erreur!")
                                .setMessage("Le temp d'attente moin de .")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        myMain.getFragmentManager().popBackStack();

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .show();



                    }else {
                        new AlertDialog.Builder(myMain)
                                .setTitle("Echec!")
                                .setMessage("Une erreur s'est produite, veuillez réessayer.")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .show();
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                if (statusCode==401)
                    new AlertDialog.Builder(act.getActivity())
                            .setTitle("Attention")
                            .setMessage("Votre session a expiré, veuillez reconnecter")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing

                                    Intent i=new Intent(act.getActivity(),LoginActivity.class);
                                    act.startActivity(i);
                                    act.getActivity().getFragmentManager().popBackStack();;
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                else
                    new AlertDialog.Builder(act.getActivity())
                            .setTitle("Echec")
                            .setMessage("Veuiller vérifier votre connection")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

            }

        });
    }

    void Resend(final Fragment act, String order_id){

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
Log.i("resend","");
        params.put("order_id",order_id);

        VTCRestClient.postLogin(act.getActivity(),"Reload", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("resend",""+s);
                    JSONObject obj = new JSONObject(s);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {

            }

        });
    }



    public void getpositionGPS(final Context context){


        Log.i("getosition","11111111");


        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

        boolean gps_enabled = false;
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {

        }

        if(!gps_enabled ) {
            // notify user
            final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setTitle("Echec");
            dialog.setMessage("Localisation not activated");
            dialog.setPositiveButton("activate", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                    //get gps
                }
            });
            dialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    paramDialogInterface.cancel();
                }
            });
            dialog.show();
        }

        //locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Log.i("getosition","11111111");
        if (checkLocationPermission(context)) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.i("getosition", "2222222");
                //Request location updates:
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 30000, 0,
                        new LocationListener() {
                            @Override
                            public void onLocationChanged(Location location) {

                                Log.i("getosition", "4444444");
                                Geocoder gcd = new Geocoder(context.getApplicationContext(), Locale.getDefault());
                                List<Address> addresses;
                                String address = null;
                                try {
                                    addresses = (ArrayList) gcd.getFromLocation(location.getLatitude(),
                                            location.getLongitude(), 1);
                                    if (addresses.size() > 0) {
                                        Log.i("locationposition", addresses.get(0).getAddressLine(0) + "," + addresses.get(0).getAddressLine(1) + "," + addresses.get(0).getAddressLine(2));
                                        address = addresses.get(0).getAddressLine(0) + ", " + addresses.get(0).getAddressLine(1) + ", " + addresses.get(0).getAddressLine(2);

                                    }

                                    if (newpos == null)
                                        newpos = new JSONObject();

                                    newpos.put("longitude", location.getLongitude());
                                    newpos.put("latitude", location.getLatitude());

                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Log.i("will change location", "yes");
                                Log.i("new lat", "" + location.getLatitude());
                                Log.i("new lng", "" + location.getLongitude());
                                // sendNewLocation(location.getLatitude(),location.getLongitude(),address);



                              /*  try {
                                    //calculateDistanceadresses(dataobj.getString("latitude"), dataobj.getString("longitude"),location.getLatitude(), location.getLongitude());

                                   // newpos.put("longitude", location.getLongitude());
                                    //newpos.put("latitude", location.getLatitude());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }*/


                            }

                            @Override
                            public void onStatusChanged(String provider, int status, Bundle extras) {
                                Log.i("onStatusChanged", "yeees");
                            }

                            @Override
                            public void onProviderEnabled(String provider) {
                                Log.i("onProviderEnabled", "yeees");
                            }

                            @Override
                            public void onProviderDisabled(String provider) {
                                Log.i("onProviderDisabled", "yeees");
                            }
                        });

                Log.i("getosition", "333333");

            }



        }else{
            Log.i("getosition","99999999");
            //Toast.makeText(DemandeExpress.this.getActivity(), "Location not found", Toast.LENGTH_SHORT).show();

        }

        // return newpos;

    }

    public boolean checkLocationPermission(final Context context) {
        if (ContextCompat.checkSelfPermission(context,android.Manifest.permission. ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                    android.Manifest.permission. ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(context)
                        .setTitle("no permission")
                        .setMessage("")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions((Activity) context,
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{android.Manifest.permission. ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


}