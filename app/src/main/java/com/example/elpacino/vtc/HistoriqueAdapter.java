package com.example.elpacino.vtc;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by elpacino on 20/07/16.
 */
public class HistoriqueAdapter extends BaseAdapter {

    JSONArray myList;
    Context context;
    int myTag;
    private static LayoutInflater inflater = null;

    public HistoriqueAdapter(Context c,JSONArray list,int tag ) {
        this.myList=list;
        context=c;
        myTag=tag;
        Log.i("AskAtinusAdapter", myList.toString());

        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getCount() {
        return myList.length()+2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (position == 0) {
            vi = inflater.inflate(R.layout.cell5, null);

        }else if (position == myList.length()+1){
            vi = inflater.inflate(R.layout.cell3, null);
            TextView total=(TextView)vi.findViewById(R.id.total);

            float somme=0;
            for (int i=0;i<myList.length();i++){
                try {
                    JSONObject object = myList.getJSONObject(i);
                    float c=0;
                    if (!object.getString("final_public_price").isEmpty())
                        c=Float.parseFloat(object.getString("final_public_price"));
                    somme=somme+c;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            total.setText(""+String.format("%.2f", somme)+"€");

        }else{
            vi = inflater.inflate(R.layout.cell4, null);
            TextView jour=(TextView)vi.findViewById(R.id.jour);
            TextView heure=(TextView)vi.findViewById(R.id.heure);
            TextView prix=(TextView)vi.findViewById(R.id.prix);

            try {
                JSONObject object = myList.getJSONObject(position-1);
                if (myTag==1){

                    jour.setText(object.getString("jour"));
                }else {

                   jour.setText(object.getString("jour")+" "+object.getString("jour1"));
                }
                heure.setText(object.getString("heur")+"h"+object.getString("minute"));

                if (!object.isNull("final_public_price")){
                    float price=0;
                    if (!object.getString("final_public_price").isEmpty())
                        price=Float.parseFloat(object.getString("final_public_price"));
                    String str = String.format("%.2f", price);
                    prix.setText(str+"€");
                }else
                    prix.setText("--");

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return vi;
    }

    private Date stringToDate(String aDate,String aFormat) {

        if(aDate==null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        Date stringDate = simpledateformat.parse(aDate, pos);
        return stringDate;

    }
}
