package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by elpacino on 23/08/16.
 */
public class PriseEnCharge extends Fragment {

    JSONObject dataobj;
    String demandeReservation;
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.prise_en_charge, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState){     // Defines the xml file for the fragment

        Button Prise_en_charge=(Button)view.findViewById(R.id.sur_site);
        Button probleme=(Button)view.findViewById(R.id.probl);

        final Bundle bundle = getArguments();
        if (bundle!=null) {
            demandeReservation = bundle.getString("notif_data", "default");
            Log.i("messageVTC", "DemandeReservation.notif_data :" + demandeReservation);

            try {
                dataobj = new JSONObject(demandeReservation);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Prise_en_charge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent;

                Voyageur fragment=new Voyageur();
                Bundle b=new Bundle();
                b.putString("fromExpress","no");
                b.putString("notif",bundle.getString("from_notification", "default"));
                b.putString("data",demandeReservation);
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment).commit();

            }
        });

        probleme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                {

                    SignalerProbleme fragg = new SignalerProbleme();
                    Bundle bundle=new Bundle();

                    try {
                        bundle.putString("offre_id",dataobj.getString("order_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    fragg.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragg).commit();


                }
            }
        });

    }
}