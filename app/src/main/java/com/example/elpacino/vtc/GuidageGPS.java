package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import android.location.LocationListener;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;



import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by elpacino on 03/08/16.
 */
//public class GuidageGPS extends AppCompatActivity implements RoutingListener, OnMapReadyCallback
public class GuidageGPS extends Fragment{

    GoogleMap map;
    private List<Polyline> polylines,Driverpolylines;
    SupportMapFragment mMap;
    String latitude, longitude;
    float distance;
    JSONObject obj;
    JSONObject order_json;
    String adr_dest;
    LatLng start;
    LatLng end;
    String attenteprix="0";
    String pricetotal="0";
    String old="0";
    Boolean routeTraced=false;
    private Projection projection;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.guidage_gps, parent, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {     // Defines the xml file for the fragment

        polylines = new ArrayList<>();
        Driverpolylines = new ArrayList<>();
        final Bundle bundle = getArguments();

        if (bundle != null) {

            try {
                String adr_data = bundle.getString("adr_data", "default");
                String coordinates_string = bundle.getString("coordinates", "default");
                String address_dest = bundle.getString("address_dest", "default");

                Log.i("coordinates", coordinates_string);
                Log.i("address_dest", address_dest);

                obj = new JSONObject(adr_data);
                Log.i("data11111122222", obj.toString());

                attenteprix = obj.getString("attenteprix");
                Log.i("attenteprixgps",attenteprix);
                pricetotal = obj.getString("pricetotal");
                old = obj.getString("old");

                order_json = new JSONObject(obj.getString("user_data"));
                adr_dest = obj.getString("address_dest");

                longitude = obj.getString("log");
                latitude = obj.getString("lat");

                if (longitude == null || latitude == null) {
                    end =new LatLng(Double.parseDouble(order_json.getString("latitude_arr")),Double.parseDouble(order_json.getString("longitude_arr")));
                } else {
                    end =new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));
                }

                Log.i("end latlng",end.toString());

                start = new LatLng(Double.parseDouble(order_json.getString("latitude_dep")), Double.parseDouble(order_json.getString("longitude_dep")));


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

       view.findViewById(R.id.open_waze).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String uri = "waze://?ll="+end.latitude+","+end.longitude+"&navigate=yes";
                    Log.i("end point",uri);
                    startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
                }catch ( ActivityNotFoundException ex  )
                {

                    Intent intent =
                            new Intent( Intent.ACTION_VIEW, Uri.parse( "https://play.google.com/store/apps/details?id=com.waze" ) );
                    startActivity(intent);
                }

            }

        });

        Button dest_atteinte = (Button)view. findViewById(R.id.dest_atteinte);
        dest_atteinte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Payement fragment=new Payement();
                Bundle b=new Bundle();

                try {
                    JSONObject map = new JSONObject();
                    map.put("distance", "" + distance);
                    map.put("user", order_json);


                    map.put("address_dest", adr_dest);
                    b.putString("my_data", map.toString());
                    b.putString("attenteprix", attenteprix);
                    Log.i("attenteprixgps",attenteprix);

                    b.putString("pricetotal", pricetotal);
                    b.putString("old", old);
                    Log.i("my_data_GPS", map.toString());
                    fragment.setArguments(b);
                    FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment).commit();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });

        Button probleme = (Button)view. findViewById(R.id.prob);

        probleme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                {
                    try {

                        SignalerProbleme fragg = new SignalerProbleme();
                        Bundle bundle=new Bundle();

                        bundle.putString("offre_id",""+ order_json.getInt("order_id"));

                        fragg.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, fragg).commit();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(getActivity()).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "GuidageGPS Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.elpacino.vtc/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "GuidageGPS Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.elpacino.vtc/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
