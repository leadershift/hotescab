package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

/**
 * Created by elpacino on 29/09/16.
 */
public class MesInfos extends Fragment {


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.mes_infos, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState) {     // Defines the xml file for the fragment

        TextView text=(TextView)view.findViewById(R.id.nom);
        TextView email=(TextView)view.findViewById(R.id.email);
        TextView phone=(TextView)view.findViewById(R.id.phone);

        SharedPreferences preferences =getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String str = preferences.getString("user", "");
        try {
            JSONObject user=new JSONObject(str);
            Log.i("user",user.toString());
            text.setText(user.getString("firstname")+" "+user.getString("lastname"));
            phone.setText(user.getString("phone"));
            email.setText(user.getString("email"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
