package com.example.elpacino.vtc;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

/**
 * Created by elpacino on 12/07/16.
 */
public class CompteActivity extends Fragment{
    String reservations;
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.compte, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        Button forgetten = (Button) view.findViewById(R.id.historique);
        Button info = (Button) view.findViewById(R.id.info);
        Button planning = (Button) view.findViewById(R.id.planning);
        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            reservations = bundle.getString("reservations", "default");
            Log.i("reservations", reservations);
        }


        planning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SharedPreferences preferences = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
                String str = preferences.getString("user", "");
                Log.i("str", str);

                JSONObject user_json = null;
                try {
                    user_json = new JSONObject(str);
                    getPlanning(user_json.getString("id").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MesInfos frag = new MesInfos();
                Bundle bundle=new Bundle();

                frag.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, frag).commit();



            }
        });

        forgetten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();

                SharedPreferences preferences = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
                String str = preferences.getString("user", "");
                try {
                    JSONObject json = new JSONObject(str);
                    params.put("user_id", json.getString("id").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                VTCRestClient.postLogin(getActivity(), "VTCHistorique", params, new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                        String s = null;
                        try {
                            s = new String(responseBody, "UTF-8");
                            Log.i("Message from Server", s);

                            JSONObject obj = new JSONObject(s);
                            HistoriqueActivity frag = new HistoriqueActivity();
                            if (obj.getString("result").toString().matches("no historique")) {
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("Message d'erreur")
                                        .setMessage("Echec, vous n'avez pas d'historique")
                                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // do nothing
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();

                            } else {
                                HistoriqueActivity fragment = new HistoriqueActivity();
                                // if (obj.getString("result").toString().matches("no historique")) {

                                //} else {

                                HistoriqueActivity fragg = new HistoriqueActivity();
                                Bundle bundle=new Bundle();
                                bundle.putString("hist", obj.getString("result").toString());

                                fragment.setArguments(bundle);
                                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.container, fragment).commit();
                            }


                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                        if (statusCode == 401)
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Attention")
                                    .setMessage("Votre session a expiré, veuillez reconnecter")
                                    .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing

                                            Intent i = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(i);
                                            getActivity().finish();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        else
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Echec")
                                    .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                                    .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                    }

                });

            }

        });

    }

    public void getPlanning(String user_id){

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("user_id",user_id);

        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "", "Loading", true);

        VTCRestClient.postLogin(getActivity(),"VTCMyReservation", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                if (progressDialog != null)
                progressDialog.dismiss();
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("Message from Server", s);

                    try {
                        JSONObject myResult= new JSONObject(s);
                        MesReservations frag = new MesReservations();
                        Bundle bundle=new Bundle();
                        bundle.putString("reservations",myResult.getString("result").toString());

                        frag.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, frag).commit();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    MesReservations frag = new MesReservations();


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {

                if (progressDialog != null)
                progressDialog.dismiss();
                if (statusCode==401)
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Attention")
                            .setMessage("Votre session a expiré, veuillez reconnecter")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing

                                    Intent i=new Intent(getActivity(),LoginActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                else
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Echec")
                            .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
            }

        });

    }
}
