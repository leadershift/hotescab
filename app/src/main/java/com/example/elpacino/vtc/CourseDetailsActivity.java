package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by elpacino on 20/07/16.
 */
public class CourseDetailsActivity extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.course_detail, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState) {     // Defines the xml file for the fragment


        TextView code = (TextView)view.findViewById(R.id.code);
        TextView adr = (TextView)view.findViewById(R.id.address);
        TextView name = (TextView)view.findViewById(R.id.user);
        TextView date = (TextView)view.findViewById(R.id.date);
        TextView montant = (TextView)view.findViewById(R.id.prix);
        TextView paiement = (TextView)view.findViewById(R.id.paiement);

        final Bundle bundle = getArguments();
        String str = bundle.getString("dic");
        try {
            JSONObject dict=new JSONObject(str);
            SharedPreferences preferences =getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
            String user = preferences.getString("user", "");
            JSONObject user_json=new JSONObject(user);
            code.setText("Code réception :"+ user_json.getString("code"));
            adr.setText(dict.getString("adressdep").toString());
            String nom="";
            if (dict.has("voyageur"))
                nom = dict.getJSONObject("voyageur").getString("firstname") + " " + dict.getJSONObject("voyageur").getString("lastname");
            else
                nom = dict.getJSONObject("receptif").getString("firstname") + " " + dict.getJSONObject("receptif").getString("lastname");

            name.setText(nom);

          //  name.setText(dict.getString("firstname").toString()+" "+dict.getString("lastname").toString());
    /*        Date mois = stringToDate(dict.getString("mois"), "M");
            Log.i("mois")
            String moisString=new SimpleDateFormat("MMMM").format(mois);*/
            String dateString=dict.getString("jour")+" "+dict.getString("jour1")+" "+dict.getString("mois")+" "+dict.getString("anne");
            date.setText(dateString);

            float f=Float.parseFloat(dict.getString("final_public_price"));
            montant.setText("Montant : "+String.format("%.2f", f)+"€");

            int pay=Integer.parseInt(dict.getString("paiment_mode"));
            String payment="";
            switch (pay){
                case 1:
                    payment="Espèces";
                    break;
                case 2:
                    payment="CB avec TPE";
                    break;
                case 3:
                    payment="Sur la note du réceptif";
                    break;
            }

            paiement.setText("Mode de paiement : "+payment);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Date stringToDate(String aDate,String aFormat) {

        if(aDate==null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        Date stringDate = simpledateformat.parse(aDate, pos);
        return stringDate;
    }
}