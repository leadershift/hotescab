package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import static com.example.elpacino.vtc.R.styleable.AlertDialog;

/**
 * Created by elpacino on 23/08/16.
 */
public class ReservationConfirm extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.confirmation, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState){     // Defines the xml file for the fragment


        Button confirmer = (Button)view. findViewById(R.id.confirm_reserv);
        final TextView adresse = (TextView)view. findViewById(R.id.adr);
        final TextView date = (TextView)view. findViewById(R.id.date);


        final Bundle bundle = getArguments();
        if (bundle != null) {
            final String demandeReservation = bundle.getString("notif_data", "default");
            Log.i("messageVTC", "DemandeReservation.notif_data :" + demandeReservation);

            try {
                JSONObject obj = new JSONObject(demandeReservation);
                adresse.setText(obj.getString("adressedep"));

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                Date datee = format.parse(obj.getString("datedep"));
                String s = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(datee);
                String time = s.substring(s.lastIndexOf(" ") + 1);
                Log.i("time", time.toString());

                date.setText( time);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }


            confirmer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    confirmReservation(demandeReservation);

                }

            });

        }
    }

    void confirmReservation(final String offre){

        try {
            final JSONObject dataobj = new JSONObject(offre);
            Log.i("dataobj",dataobj.toString());

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        SharedPreferences preferences =getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String str = preferences.getString("user", "");

            JSONObject json = new JSONObject(str);
            params.put("user_id", json.getString("id").toString());
            params.put("order_id",dataobj.getString("order_id") );
            Log.i("params",params.toString());

            VTCRestClient.postLogin(getActivity(),"VTCConfirmation", params, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                    String s = null;
                    try {
                        s = new String(responseBody, "UTF-8");
                        Log.i("messageVTC11111","Message from Server "+s);

                        JSONObject obj = new JSONObject(s);
                        String retour=obj.getString("result");

                        if (retour instanceof String){

                            if (retour.equals("success")){
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("Succès")
                                        .setMessage("Merci")
                                        .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                AccueilActivity frag = new AccueilActivity();
                                                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                                                fragmentTransaction.replace(R.id.container, frag);
                                                // fragmentTransaction.addToBackStack(null);
                                                fragmentTransaction.commit();// do nothing
                                                String dtdepart = null;

                                                try {

                                                    Intent localIntent=new Intent(getActivity(), MainActivity.class);
                                                    localIntent.putExtra("notif_data", offre);
                                                    localIntent.putExtra("from_notification", "yes");

                                                    PendingIntent pendingIntent1 = PendingIntent.getActivity(getActivity(), 0, localIntent, PendingIntent.FLAG_ONE_SHOT);

                                                    Uri defaultSoundUri1 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                                    final NotificationCompat.Builder notificationBuilder1 = new NotificationCompat.Builder(getActivity())
                                                            .setSmallIcon(R.drawable.ic_launcher)
                                                            .setContentTitle("Hotels Cab")
                                                            .setContentText("temps de prise en charge")
                                                            .setAutoCancel(true)
                                                            .setSound(defaultSoundUri1)
                                                            .setContentIntent(pendingIntent1);

        final NotificationManager notificationManager1 = (NotificationManager)getActivity(). getSystemService(Context.NOTIFICATION_SERVICE);

                                                    if (dataobj.getString("datedarr").toString().equals(dataobj.getString("datedep").toString())){
                                                        notificationManager1.notify(0, notificationBuilder1.build());
                                                    }else {
                                                        dtdepart = dataobj.getString("datedep").substring(dataobj.getString("datedep").lastIndexOf(" ") + 1);
                                                        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                                                        Date endDate = format.parse(dtdepart);
                                                        Log.i("endDate", "" + endDate);
                                                        String currentTime = format.format(new Date());
                                                        Date startDate = format.parse(currentTime);
                                                        Log.i("startDate", "" + startDate);

                                                        long diffInMs = endDate.getTime() - startDate.getTime();

                                                        long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
                                                        Log.i("diffInSec", "" + diffInSec);

                                                        if (diffInSec>=0){

                                                            final Timer myTimer = new Timer();
                                                            MySingleton.getInstance().setTimer(myTimer);

                                                            myTimer.schedule(new TimerTask() {
                                                                @Override
                                                                public void run() {
                                                                    Log.i("notificationpriseench","dsdf");

                                                                    this.cancel();
                                                                   /* if (MainActivity.getInstance() instanceof MainActivity) {
                                                                        Log.i("messageVTC", "exist");
                                                                        final MainActivity activity = MainActivity.getInstance();

                                                                        activity.changefragment1(offre, "4");

                                                                    }else {*/


                                                                    Log.i("notificationpriseench","2222222");
                                                                        notificationManager1.notify(0, notificationBuilder1.build());
                                                                  //  }
                                                                }

                                                            },diffInSec*1000);

                                                        }else {
                                                            Toast.makeText(getActivity(), "Attention, le temps de prise e charge prévu est déja passé!!", Toast.LENGTH_SHORT).show();
                                                        }


                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                } catch (ParseException e) {

                                                    e.printStackTrace();
                                                }

                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_info)
                                        .show();

                            }else  if (retour.equals("failed")){
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("Echec")
                                        .setMessage("contact l'administrateur")
                                        .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                getActivity().getFragmentManager().popBackStack();// do nothing
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();

                            }else{
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("Echec")
                                        .setMessage("contact1111 l'administrateur")
                                        .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                AccueilActivity frag = new AccueilActivity();
                                                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                                                fragmentTransaction.replace(R.id.container, frag);
                                                // fragmentTransaction.addToBackStack(null);
                                                fragmentTransaction.commit();// do nothing // do nothing
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            }

                        }else {

                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Echec")
                                    .setMessage("dataProblem")
                                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            getActivity().getFragmentManager().popBackStack();// do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                    if (statusCode==401)
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Attention")
                                .setMessage("Votre session a expiré, veuillez reconnecter")
                                .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing

                                        Intent i=new Intent(getActivity(),LoginActivity.class);
                                        startActivity(i);
                                        getActivity().finish();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    else
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Echec")
                                .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                                .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                }
            });


        }catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
