package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by elpacino on 11/07/16.
 */
public class ForgetPasswordActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgetten);

        Button valider=(Button)findViewById(R.id.valider);
        Button connexion=(Button)findViewById(R.id.conn);
        final EditText email=(EditText)findViewById(R.id.email);

        connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().toString().matches("")){
                    new AlertDialog.Builder(ForgetPasswordActivity.this)
                            .setTitle("Echec")
                            .setMessage("Veuillez taper votre email")
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }else {
                    RequestParams params = new RequestParams();

                    params.put("email", email.getText().toString());
                    // params.put("grant_type","password");
                    // params.put("client_id", "1_3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4");
                    // params.put("client_secret","4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k");

                    VTCRestClient.postLogin(ForgetPasswordActivity.this,"passOublier", params, new AsyncHttpResponseHandler(){

                        @Override
                        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                            String s = null;
                            try {
                                s = new String(responseBody, "UTF-8");
                                Log.i("Message from Server", s);
                                JSONObject obj = new JSONObject(s);
                                String str=obj.getString("result");
                                if (str.equals("no user found")){
                                    new AlertDialog.Builder(ForgetPasswordActivity.this)
                                            .setTitle("Echec")
                                            .setMessage("Votre email n'est pas valide")
                                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // do nothing
                                                }
                                            })
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }else if (str.equals("success")){

                                    new AlertDialog.Builder(ForgetPasswordActivity.this)
                                            .setTitle("Succès")
                                            .setMessage("Veuillez consulter votre boite mail pour avoir le nouveau mot de passe.")
                                            .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    finish();
                                                    // do nothing
                                                }
                                            })
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }else if(str.equals("error")) {
                                    new AlertDialog.Builder(ForgetPasswordActivity.this)
                                            .setTitle("Echec")
                                            .setMessage("Une erreur s'est poduite, veuillez réessayer plus tard.")
                                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // do nothing
                                                }
                                            })
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }

                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                            if (statusCode==401)
                                new AlertDialog.Builder(ForgetPasswordActivity.this)
                                        .setTitle("Attention")
                                        .setMessage("Votre session a expiré, veuillez reconnecter")
                                        .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // do nothing

                                                Intent i=new Intent(ForgetPasswordActivity.this,LoginActivity.class);
                                                startActivity(i);
                                                finish();
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            else
                                new AlertDialog.Builder(ForgetPasswordActivity.this)
                                        .setTitle("Echec")
                                        .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                                        .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // do nothing
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                        }
                    });
                }
            }
        });


    }

}
