package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;


/**
 * Created by elpacino on 01/07/16.
 */
public class LoginActivity extends Activity {

    ProgressDialog progressDialog;
    EditText email;
    EditText pass;
    BroadcastReceiver Registration_Broadcast_receiver;
    private boolean isReceiverRegistered;
    String token;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Button forgetten=(Button)findViewById(R.id.forget);
        Button connexion=(Button)findViewById(R.id.valider);

        Registration_Broadcast_receiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().endsWith(GCMRegistrationIntentService.REGISTRATION_SUCCESS)){
                    token=intent.getStringExtra("token");
                    //Toast.makeText(getApplicationContext(),"token:"+token,Toast.LENGTH_LONG).show();
                }else if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)){
                    //Toast.makeText(getApplicationContext(),"error",Toast.LENGTH_LONG).show();
                }else {

                }
            }
        };

        int resultCode= GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        Log.i("resultCode",""+resultCode);

        Log.i("ConnectionResult",""+ ConnectionResult.SUCCESS);
        if (ConnectionResult.SUCCESS!=resultCode){
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)){
                // Toast.makeText(getApplicationContext(),"Google Play service is not install/enabled in device",Toast.LENGTH_LONG).show();

                GooglePlayServicesUtil.showErrorNotification(resultCode,getApplicationContext());
            }else {
                 //Toast.makeText(getApplicationContext(),"This device does not support for google play service",Toast.LENGTH_LONG).show();
            }
        }else {
            Intent intent=new Intent(this, GCMRegistrationIntentService.class);
            startService(intent);
        }

        registerReceiver();

        email=(EditText)findViewById(R.id.login);
        pass=(EditText)findViewById(R.id.pass);

        forgetten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);
            }
        });

        connexion.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {



                if (email.getText().toString().matches("") || pass.getText().toString().matches("")) {

                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle("Attention")
                            .setMessage("Vous devez remplir les champs vides")
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                } else {

                    Login();

                }
            }
            });

            }

    @Override
    protected void onResume() {
        super.onResume();
        Log.w("MainActivity", "onResume");
        registerReceiver();
       /* LocalBroadcastManager.getInstance(this).registerReceiver(Registration_Broadcast_receiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(Registration_Broadcast_receiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));*/
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("")
                .setMessage("Vous êtes sur que vous voulez quitter l'application?")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("Non", null)
                .show();
    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(Registration_Broadcast_receiver,
                    new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
            isReceiverRegistered = true;
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.w("MainActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(Registration_Broadcast_receiver);
        isReceiverRegistered = false;
    }

    public void Login(){

        if (email.getText().toString().isEmpty()||pass.getText().toString().isEmpty()){

        }else

            getAuthToken(email.getText().toString(),pass.getText().toString());
          Log.i("Will Login","yes");

    }

    public void RequestLogin(){
        RequestParams params = new RequestParams();

        params.put("email", email.getText().toString());
        params.put("password", pass.getText().toString());
        // params.put("grant_type","password");
        // params.put("client_id", "1_3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4");
        // params.put("client_secret","4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k");

        Log.i("params",params.toString());
        VTCRestClient.postLogin(LoginActivity.this,"login", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("Message from Server", s);
                    JSONObject obj = new JSONObject(s);
                    String str=obj.getString("result");
                    if (str.equals("wrong password")||str.equals("no user found")){

                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Echec")
                                .setMessage("Identifiant ou mot de passe incorrects")
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }else if(str.equals("error")) {

                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Echec")
                                .setMessage("Une erreur s'est poduite, veuillez réessayer plus tard.")
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }else {

                        JSONObject user_dict=new JSONObject(str).getJSONObject("user");

                        sendToken(user_dict.getString("id"));

                        SharedPreferences preferences =getSharedPreferences("pref", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("user", user_dict.toString());
                        editor.commit();

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("from_notif","no");
                        startActivity(intent);
                        finish();
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                Log.i("statusCode",""+statusCode);

                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle("Echec")
                            .setMessage("Veuillez vérifier votre connexion.")
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
            }
        });
    }

    public void getAuthToken(String username,String password){
        AsyncHttpClient client=new AsyncHttpClient();
        client.addHeader("accept","text/html");
        Log.i("prepare ","getAuth");
       // http://151.80.19.66/vtc2/vtc2/web/app.php/

        client.get("http://office-hc.fr/oauth/v2/token?client_id=5_492s363nbywwsoosooo0004ckkwskok80g0ww4kgco4cw4ww88&client_secret=4hzqsb7i5mskswow0owsow8gcok8cg84sg4k4sw8sw8ko488so&grant_type=client_credentials", null, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String s = null;
                try {
                    Log.i("response","returned");
                    s = new String(responseBody, "UTF-8");
                    Log.i("Aouth response message", s);
                    JSONObject obj=new JSONObject(s);
                    if (obj.has("access_token")){
                        SharedPreferences sharedpreferences = getSharedPreferences("pref",MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("token", obj.getString("access_token"));
                        editor.commit();
                        RequestLogin();;

                    } else{
                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Echec")
                                .setMessage("Aucune correspondance pour l'identifiant et/ou le mot de passe.onfail")
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }



                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                try {
                    Log.i("responseBody",new String(responseBody, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Log.i("statusCode",""+statusCode);
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("Echec")
                        .setMessage("Erreur application =>  Êtes connecté au Wifi.")
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                //Toast.makeText(LoginActivity.this, "Aouth Error occurred!!"+error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void sendToken(String user_id){

        Log.i("user_id",user_id);

       // SharedPreferences preferences =getSharedPreferences("pref", Context.MODE_PRIVATE);
       // String token = preferences.getString("device_token", "");

       // String token = MySingleton.getInstance().getToken();
        //Log.i("token",token);

        RequestParams params = new RequestParams();

        params.put("token", token);
        params.put("user_id", user_id);

        VTCRestClient.postLogin(LoginActivity.this,"addVTCToken" , params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("Token response message", s);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {

            }

        });
    }
}