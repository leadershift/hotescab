package com.example.elpacino.vtc;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by WAFA on 03/05/2016.
 */
public class VTCRestClient {

  // http://151.80.19.66/vtc2/vtc2/web/app.php/api/ws/
    private static final String BASE_URL = "http://office-hc.fr/api/ws/";

private static AsyncHttpClient client = new AsyncHttpClient();
   // static String AUTORISATION ="Bearer ODJiMWJhN2JjNzAyYjFlYjhlYmViMTAwZTdjNjZkNGU0OGY0MDkzMGI5YzI2MDI0MzJhZWQ3YmU1ODk1MDI0Yw";
    //static String CONTENTTYPE="application/x-www-form-urlencoded";
        public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.get(getAbsoluteUrl(url, ""), params, responseHandler);

        }

        public static void postLogin(Activity act,String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {

            Log.i("current activity",""+act);
            SharedPreferences sharedpreferences=null;
            if (act !=null)
               sharedpreferences =act.getSharedPreferences("pref",act.MODE_PRIVATE);
            else
                sharedpreferences = MySingleton.getInstance().myMain.getSharedPreferences("pref",act.MODE_PRIVATE);

            String token = sharedpreferences.getString("token", "");

            Log.i("Token",token);
            client.addHeader("Authorization","Bearer "+token);
           // Content-Type: application/x-www-form-urlencoded
            //client.he("Authorization",AUTORISATION);
            client.addHeader("Content-Type","application/x-www-form-urlencoded");
            client.post(getAbsoluteUrl(url, token), params, responseHandler);

        }
       /* public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.post(getAbsoluteUrl(url), params, responseHandler);


        }*/
        public static void getUser(String accessToken,String url, AsyncHttpResponseHandler responseHandler) {
            client.get(getAbsoluteUrl(url, ""), responseHandler);

        }

        public static void putUser(String accessToken,String url,RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.put(getAbsoluteUrl(url, ""), params, responseHandler);

        }
        private static String getAbsoluteUrl(String relativeUrl, String token) {
            Log.i("", "Signup URL " + BASE_URL + relativeUrl.toString()+"?access_token="+token);
            return BASE_URL + relativeUrl+"?access_token="+token;


        }
}