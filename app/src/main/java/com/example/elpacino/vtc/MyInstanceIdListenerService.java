package com.example.elpacino.vtc;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by NgocTri on 4/9/2016.
 */
public class MyInstanceIdListenerService extends com.google.android.gms.iid.InstanceIDListenerService {
    /**
     * When token refresh, start service to get new token
     */
    @Override
    public void onTokenRefresh() {


        Log.i("token refreshed","true");
        Intent intent = new Intent(this, GCMRegistrationIntentService.class);
        startService(intent);
    }
}
