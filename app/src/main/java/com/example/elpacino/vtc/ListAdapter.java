package com.example.elpacino.vtc;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by elpacino on 13/07/16.
 */
public class ListAdapter extends BaseAdapter {

   JSONArray myList;
    Context context;
    private static LayoutInflater inflater = null;

    public ListAdapter(Context c,JSONArray list ) {
        this.myList=list;
        context=c;
        Log.i("AskAtinusAdapter", myList.toString());

        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getCount()
    {
        return myList.length()+2;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;


        if (position == 0) {
            vi = inflater.inflate(R.layout.cell1, null);

        }else if (position == myList.length()+1){
            vi = inflater.inflate(R.layout.cell3, null);
            TextView total=(TextView)vi.findViewById(R.id.total);

            float somme=0;
            for (int i=0;i<myList.length();i++){
                try {
                    JSONObject object = myList.getJSONObject(i);
                    float prise=0;
                    if (!object.getString("final_public_price").isEmpty())
                        prise=Float.parseFloat(object.getString("final_public_price"));

                    somme=somme+prise;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            total.setText(""+String.format("%.2f", somme)+"€");

        }else{
            vi = inflater.inflate(R.layout.cell2, null);
            TextView heure=(TextView)vi.findViewById(R.id.heure);
            TextView prix=(TextView)vi.findViewById(R.id.prix);

            try {
                JSONObject object = myList.getJSONObject(position-1);
                heure.setText(object.getString("heur")+"h"+object.getString("minute"));
                float f=0;
                if (!object.getString("final_public_price").isEmpty())
                    f=Float.parseFloat(object.getString("final_public_price"));

                String str = String.format("%.2f", f);
                prix.setText(str+"€");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            /*HashMap<String, String> map=myList.get(position-1);

            Log.i("my map", map.toString());


            TextView heure = (TextView) vi.findViewById(R.id.heure);
            heure.setText(map.get(heure));
            TextView prix = (TextView) vi.findViewById(R.id.prix);
            prix.setText(map.get(prix));
*/
        }

        return vi;
    }
}
