package com.example.elpacino.vtc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by elpacino on 03/08/16.
 */
public class ProblemeSpecifique extends Fragment {

    String offre_id;

    Fragment frag=this;
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.prob_specifique, parent, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState){     // Defines the xml file for the fragment

        Button valider=(Button)view.findViewById(R.id.envoyer);
        final EditText prob_text=(EditText)view.findViewById(R.id.prob_text);

        SharedPreferences preferences = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        final String str = preferences.getString("user", "");
        Log.i("str", str);

        final Bundle bundle = getArguments();

        if (bundle != null) {
            offre_id = bundle.getString("offre_id", "default");
        }

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (prob_text.getText().toString().matches("")){
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Attention.")
                            .setMessage("Vous devez tout d'abord indiquer votre problème.")
                            .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AccueilActivity frag = new AccueilActivity();
                                    FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.container, frag);
                                    // fragmentTransaction.addToBackStack(null);
                                    fragmentTransaction.commit();// do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                }else {
                    try {
                        final JSONObject user_json = new JSONObject(str);

                        MySingleton.getInstance().IndiquerProbleme(user_json.getString("id"),offre_id ,prob_text.getText().toString(),frag,"0");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

}
